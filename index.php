<?php

require 'vendor/autoload.php';

$app = new \Slim\App([
    'settings'=>[
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'db'=> [
            'driver'=> 'mysql',
            'host'=> 'localhost',
            'database'=> 'dicom',
            'username'=> 'root',
            'password'=> 'root',
            'charset'=> 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'=> ''
        ]
    ]
]);

// $app = new \Slim\App([
//     'settings'=>[
//         'determineRouteBeforeAppMiddleware' => false,
//         'displayErrorDetails' => true,
//         'addContentLengthHeader' => false,
//         'db'=> [
//             'driver'=> 'mysql',
//             'host'=> 'pdb7.awardspace.net',
//             'database'=> '951994_dicom',
//             'username'=> '951994_dicom',
//             'password'=> 'dicom1234',
//             'charset'=> 'utf8',
//             'collation' => 'utf8_unicode_ci',
//             'prefix'=> ''
//         ]
//     ]
// ]);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db']= function($container) use ($capsule){
    return $capsule;
};

$container['UsuariosController'] = function ($container){
    return new \mvc\Controllers\UsuariosController($container);
};

$container['PacientesController'] = function ($container){
    return new \mvc\Controllers\PacientesController($container);
};

$container['LoginsController'] = function ($container){
    return new \mvc\Controllers\LoginsController($container);
};

$container['DireccionesController'] = function ($container){
    return new \mvc\Controllers\DireccionesController($container);
};

$container['EducacionesController'] = function ($container){
    return new \mvc\Controllers\EducacionesController($container);
};

$container['HorariosController'] = function ($container){
    return new \mvc\Controllers\HorariosController($container);
};

$container['DatosController'] = function ($container){
    return new \mvc\Controllers\DatosController($container);
};

$container['TelefonosController'] = function ($container){
    return new \mvc\Controllers\TelefonosController($container);
};

$container['MailsController'] = function ($container){
    return new \mvc\Controllers\MailsController($container);
};

$container['OperacionesController'] = function ($container){
    return new \mvc\Controllers\OperacionesController($container);
};

$container['DatadicomsController'] = function ($container){
    return new \mvc\Controllers\DatadicomsController($container);
};

$container['StudiesController'] = function ($container){
    return new \mvc\Controllers\StudiesController($container);
};

$container['SeriesController'] = function ($container){
    return new \mvc\Controllers\SeriesController($container);
};

$container['InstancesController'] = function ($container){
    return new \mvc\Controllers\InstancesController($container);
};

$container['PruebaDicomController'] = function ($container){
    return new \mvc\Controllers\PruebaDicomController($container);
};

$usuariosRoutes = new \mvc\Routes\UsuariosRoutes($app);
$usuariosRoutes->setRoutes($app);

$pacientesRoutes = new \mvc\Routes\PacientesRoutes($app);
$pacientesRoutes->setRoutes($app);

$loginsRoutes = new \mvc\Routes\LoginsRoutes($app);
$loginsRoutes->setRoutes($app);

$direccionesRoutes = new \mvc\Routes\DireccionesRoutes($app);
$direccionesRoutes->setRoutes($app);

$educacionesRoutes = new \mvc\Routes\EducacionesRoutes($app);
$educacionesRoutes->setRoutes($app);

$horariosRoutes = new \mvc\Routes\HorariosRoutes($app);
$horariosRoutes->setRoutes($app);

$datosRoutes = new \mvc\Routes\DatosRoutes($app);
$datosRoutes->setRoutes($app);

$telefonosRoutes = new \mvc\Routes\TelefonosRoutes($app);
$telefonosRoutes->setRoutes($app);

$mailsRoutes = new \mvc\Routes\MailsRoutes($app);
$mailsRoutes->setRoutes($app);

$operacionesRoutes = new \mvc\Routes\OperacionesRoutes($app);
$operacionesRoutes->setRoutes($app);

$datadicomsRoutes = new \mvc\Routes\DatadicomsRoutes($app);
$datadicomsRoutes->setRoutes($app);

$studiesRoutes = new \mvc\Routes\StudiesRoutes($app);
$studiesRoutes->setRoutes($app);

$seriesRoutes = new \mvc\Routes\SeriesRoutes($app);
$seriesRoutes->setRoutes($app);

$instancesRoutes = new \mvc\Routes\InstancesRoutes($app);
$instancesRoutes->setRoutes($app);

$pruebadicomRoutes = new \mvc\Routes\PruebaDicomRoutes($app);
$pruebadicomRoutes->setRoutes($app);

$app->run();