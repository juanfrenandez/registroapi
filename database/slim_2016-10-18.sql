# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28)
# Database: slim
# Generation Time: 2016-10-19 00:31:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table direccion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `direccion`;

CREATE TABLE `direccion` (
  `id_direccion` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` int(11) NOT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_direccion`),
  KEY `fk_direccion_registro_idx` (`registro_id`),
  CONSTRAINT `fk_direccion_registro` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;

INSERT INTO `direccion` (`id_direccion`, `calle`, `numero`, `colonia`, `ciudad`, `estado`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(2,'siempreviva',23,NULL,NULL,NULL,1,'2016-10-17 20:14:35','2016-10-17 20:14:35'),
	(5,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:26:04','2016-10-17 20:26:04'),
	(6,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:41:45','2016-10-17 20:41:45'),
	(7,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:42:33','2016-10-17 20:42:33'),
	(8,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:45:50','2016-10-17 20:45:50'),
	(9,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:48:39','2016-10-17 20:48:39'),
	(10,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:49:30','2016-10-17 20:49:30'),
	(11,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:52:16','2016-10-17 20:52:16'),
	(12,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 20:53:03','2016-10-17 20:53:03'),
	(13,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:05:13','2016-10-17 21:05:13'),
	(14,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:11:01','2016-10-17 21:11:01'),
	(15,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:13:37','2016-10-17 21:13:37'),
	(16,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:15:06','2016-10-17 21:15:06'),
	(17,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:16:45','2016-10-17 21:16:45'),
	(18,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:18:05','2016-10-17 21:18:05'),
	(19,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:18:33','2016-10-17 21:18:33'),
	(20,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:21:53','2016-10-17 21:21:53'),
	(21,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:22:05','2016-10-17 21:22:05'),
	(22,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:22:19','2016-10-17 21:22:19'),
	(23,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:23:30','2016-10-17 21:23:30'),
	(24,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:23:43','2016-10-17 21:23:43'),
	(25,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:24:04','2016-10-17 21:24:04'),
	(26,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:25:05','2016-10-17 21:25:05'),
	(27,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:25:19','2016-10-17 21:25:19'),
	(28,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:28:08','2016-10-17 21:28:08'),
	(29,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:28:21','2016-10-17 21:28:21'),
	(30,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:29:07','2016-10-17 21:29:07'),
	(31,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:29:41','2016-10-17 21:29:41'),
	(32,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:31:39','2016-10-17 21:31:39'),
	(33,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:31:49','2016-10-17 21:31:49'),
	(34,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:32:20','2016-10-17 21:32:20'),
	(35,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:42:28','2016-10-17 21:42:28'),
	(36,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:43:29','2016-10-17 21:43:29'),
	(37,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:48:08','2016-10-17 21:48:08'),
	(38,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:48:23','2016-10-17 21:48:23'),
	(39,'siempreviva',23,NULL,NULL,NULL,4,'2016-10-17 21:49:14','2016-10-17 21:49:14'),
	(40,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:52:16','2016-10-17 21:52:16'),
	(41,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:53:01','2016-10-17 21:53:01'),
	(42,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:53:09','2016-10-17 21:53:09'),
	(43,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:55:30','2016-10-17 21:55:30'),
	(44,'dwedwed',53,NULL,NULL,NULL,4,'2016-10-17 21:56:23','2016-10-17 21:56:23'),
	(46,'chida',0,NULL,NULL,NULL,70,'2016-10-17 22:00:00','2016-10-17 22:00:00'),
	(47,'dwedwed',4234,NULL,NULL,NULL,70,'2016-10-17 22:04:42','2016-10-17 22:04:42'),
	(48,'dwedwed',4234,NULL,NULL,NULL,70,'2016-10-17 22:05:37','2016-10-17 22:05:37'),
	(49,'namvre',334,NULL,NULL,NULL,69,'2016-10-18 00:15:32','2016-10-18 00:15:32'),
	(50,'namvre',334,NULL,NULL,NULL,69,'2016-10-18 00:17:08','2016-10-18 00:17:08'),
	(51,'namvre',334,NULL,NULL,NULL,69,'2016-10-18 00:17:22','2016-10-18 00:17:22'),
	(52,'namvre',334,NULL,NULL,NULL,69,'2016-10-18 00:18:04','2016-10-18 00:18:04'),
	(53,'nueva',34,NULL,NULL,NULL,74,'2016-10-18 22:13:44','2016-10-18 22:13:44'),
	(54,'nueva',34,NULL,NULL,NULL,74,'2016-10-18 22:38:22','2016-10-18 22:38:22'),
	(55,'nueva',34,NULL,NULL,NULL,74,'2016-10-18 22:41:01','2016-10-18 22:41:01'),
	(56,'nueva',34,NULL,NULL,NULL,74,'2016-10-18 23:13:05','2016-10-18 23:13:05'),
	(57,'nueva',34,NULL,NULL,NULL,75,'2016-10-18 23:13:15','2016-10-18 23:13:15'),
	(58,'nuevae23e3222',3,NULL,NULL,NULL,75,'2016-10-18 23:14:01','2016-10-18 23:14:01');

/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table educacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `educacion`;

CREATE TABLE `educacion` (
  `id_educacion` int(11) NOT NULL AUTO_INCREMENT,
  `espacialidad` varchar(100) NOT NULL,
  `escuela` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `inicio` varchar(30) NOT NULL,
  `termino` varchar(30) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id_educacion`),
  KEY `fk_educacion_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_educacion_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table registro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registro`;

CREATE TABLE `registro` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `token` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;

INSERT INTO `registro` (`id`, `user`, `password`, `updated_at`, `created_at`, `token`)
VALUES
	(1,'apiid2','fsdfsdf','2016-10-18 01:43:47',NULL,NULL),
	(4,'cambiosf34f33f13f3ff3fefef324f','4234234','2016-10-17 23:09:04',NULL,NULL),
	(5,'fr43','4234234','2016-10-18 02:04:40',NULL,NULL),
	(6,'fsdfrewf','ferfrefer',NULL,NULL,NULL),
	(8,'zzzzzzzzzzzzzzzzz','mañana','2016-10-18 02:11:30',NULL,NULL),
	(9,'hola','mañana',NULL,NULL,NULL),
	(11,'holas','nombre',NULL,NULL,NULL),
	(12,'4r','4r',NULL,NULL,NULL),
	(13,'nuevo','nuevo','2016-10-18 20:58:51',NULL,'95a8bd14466ff19bc34bbd9d0cbdd113a32147f90b3656fe39de80ceb89bd3fd'),
	(14,'macho','nuevo',NULL,NULL,NULL),
	(15,'nuevo','1234',NULL,NULL,NULL),
	(16,'John','','2016-10-13 19:54:49','2016-10-13 19:54:49',NULL),
	(17,'juan','John','2016-10-18 20:39:21','2016-10-13 19:54:49','e5c3628d0f4a2be57608202c98875fd1adc51c66ffa242087b16b2b2ca3f37be'),
	(18,'John','','2016-10-13 19:55:16','2016-10-13 19:55:16',NULL),
	(19,'nuevo','John','2016-10-18 20:41:35','2016-10-13 19:55:16','9b85759cb42ef79f18028d5bfc0e6dc449984709d5c7ed3314cb29f6eeb0c64c'),
	(20,'john@foo.com','','2016-10-13 23:31:36','2016-10-13 19:55:55',NULL),
	(21,'nuevo','John','2016-10-18 20:54:28','2016-10-13 19:55:55','d694c1e477684cb51d854902b5b4ca35ed9f83047193bc75982a751c387e1eb1'),
	(22,'John','','2016-10-13 19:56:38','2016-10-13 19:56:38',NULL),
	(23,'nuevo','John','2016-10-18 20:54:31','2016-10-13 19:56:38','390283c2e81a745cba90371ec12486b38c75a7a2b61eeaac1061f32a7f47b381'),
	(24,'John','','2016-10-13 19:58:16','2016-10-13 19:58:16',NULL),
	(25,'nuevo','John','2016-10-18 20:55:08','2016-10-13 19:58:16','da862b782b4d574137d6f44f97b4fd3720c9dec95e0bea80c72d457639330560'),
	(26,'John','','2016-10-13 19:59:23','2016-10-13 19:59:23',NULL),
	(27,'nuevo','John','2016-10-18 20:55:33','2016-10-13 19:59:23','74bf7d20a4afe1cff44e4fab736ae802a33422efc844b93e78e034303475f82e'),
	(28,'John','','2016-10-13 20:30:21','2016-10-13 20:30:21',NULL),
	(29,'nuevo','John','2016-10-18 20:55:34','2016-10-13 20:30:21','7e458dfd52a37ac693ceb4a6de2bc96efedcfe90a84eecd65fe1931f711b1493'),
	(30,'John','John','2016-10-13 20:31:29','2016-10-13 20:31:29',NULL),
	(31,'John','John','2016-10-13 20:35:16','2016-10-13 20:35:16',NULL),
	(32,'John','John','2016-10-13 20:36:55','2016-10-13 20:36:55',NULL),
	(33,'vientos de cambio','','2016-10-13 23:36:23','2016-10-13 20:50:58',NULL),
	(34,'1','','2016-10-13 20:53:55','2016-10-13 20:53:55',NULL),
	(35,'1','','2016-10-13 20:54:07','2016-10-13 20:54:07',NULL),
	(36,'1','','2016-10-13 20:55:13','2016-10-13 20:55:13',NULL),
	(37,'HTTP/1.1 200 OK\nContent-Type: ','','2016-10-13 21:05:17','2016-10-13 21:05:17',NULL),
	(38,'HTTP/1.1 200 OK\nContent-Type: ','','2016-10-13 21:06:55','2016-10-13 21:06:55',NULL),
	(39,'HTTP/1.1 200 OK\nContent-Type: ','','2016-10-13 21:08:19','2016-10-13 21:08:19',NULL),
	(40,'$dataJson','','2016-10-13 21:08:57','2016-10-13 21:08:57',NULL),
	(41,'$dataJson','','2016-10-13 21:10:39','2016-10-13 21:10:39',NULL),
	(42,'$dataJson','','2016-10-13 21:14:53','2016-10-13 21:14:53',NULL),
	(43,'fdlnbs','','2016-10-13 21:32:22','2016-10-13 21:32:22',NULL),
	(44,'fdlnbs','','2016-10-13 21:34:00','2016-10-13 21:34:00',NULL),
	(45,'fdlnbs','','2016-10-13 21:34:59','2016-10-13 21:34:59',NULL),
	(46,'fdlnbs','','2016-10-13 21:35:47','2016-10-13 21:35:47',NULL),
	(47,'fdlnbs','','2016-10-13 21:37:40','2016-10-13 21:37:40',NULL),
	(48,'fdlnbs','','2016-10-13 21:37:54','2016-10-13 21:37:54',NULL),
	(49,'fdlnbs','','2016-10-13 21:40:23','2016-10-13 21:40:23',NULL),
	(50,'fdlnbs','','2016-10-13 21:42:47','2016-10-13 21:42:47',NULL),
	(51,'fdlnbs','','2016-10-13 21:44:02','2016-10-13 21:44:02',NULL),
	(52,'fdlnbs','','2016-10-13 21:45:59','2016-10-13 21:45:59',NULL),
	(53,'fdlnbs','','2016-10-13 21:46:43','2016-10-13 21:46:43',NULL),
	(54,'fdlnbs','','2016-10-13 21:47:26','2016-10-13 21:47:26',NULL),
	(55,'fdlnbs','','2016-10-13 21:48:35','2016-10-13 21:48:35',NULL),
	(56,'fdlnbs','','2016-10-13 21:49:36','2016-10-13 21:49:36',NULL),
	(57,'fdlnbs','','2016-10-13 21:50:06','2016-10-13 21:50:06',NULL),
	(58,'fdlnbs','','2016-10-13 21:50:31','2016-10-13 21:50:31',NULL),
	(59,'fdlnbs','','2016-10-13 21:50:49','2016-10-13 21:50:49',NULL),
	(60,'fewfewfewfe','','2016-10-13 21:52:04','2016-10-13 21:52:04',NULL),
	(61,'fewfewfewfe','ferfwefewefr3333','2016-10-13 21:53:00','2016-10-13 21:53:00',NULL),
	(62,'cambios','nombresifunciona','2016-10-13 21:54:03','2016-10-13 21:54:03',NULL),
	(63,'vientos de cambio','funciona','2016-10-13 23:51:07','2016-10-13 23:51:07',NULL),
	(64,'vientos de cambio','$2y$10$6evlDyIDSZ6D2OGBqZvcfuQ','2016-10-13 23:58:21','2016-10-13 23:58:21',NULL),
	(65,'vientos de cambio','$2y$10$FLXDfNKQ9mOUGO/wQysh9uG','2016-10-14 02:03:29','2016-10-14 02:03:29',NULL),
	(66,'vientos','$2y$10$hCs8HSwbLtVvaM2M0wTqvOQ','2016-10-14 02:03:56','2016-10-14 02:03:56',NULL),
	(67,'juan','$2y$10$WgryaiGokH5BuDFgeiRv1Oe','2016-10-15 00:09:36','2016-10-15 00:09:36',NULL),
	(69,'joseju','$2y$10$.gPv2rGODYarcv2X3FDPKeZHPlw/MC7FRiyp6VA4DwKX3JXhc/1bm','2016-10-18 02:13:18','2016-10-15 00:22:10',NULL),
	(70,'apiid2','$2y$10$XPBCu4/XKd/m/yu0UnPMfu6LSeREFPwWPZpUOvNqVOpDZJvpLXnQ2','2016-10-18 01:47:54','2016-10-17 18:28:56',NULL),
	(72,'juan','$2y$10$RIgrTWjDT3PLcEMuxl149OWagYqiZnh3hT0PDeo2ldRrRNDfJwniy','2016-10-18 17:55:09','2016-10-18 17:55:09',NULL),
	(74,'chido','$2y$10$nkHWvqdnnvtd3W2QEFhjYOybmQ2os0alETy0IzrfZj7zSOKm9lvmC','2016-10-18 21:54:19','2016-10-18 19:35:39','51b95de05de568be2fa1918e9d8d1a1d0d797f8e2bf493043d1232aed565e77c'),
	(75,'chido','$2y$10$bxmWvkBXKue6owx8y3VFRuUgDv0Oyqq4FtTcF0d.2qX5VwUFSYCGy','2016-10-18 23:07:45','2016-10-18 23:00:23','4ee58b073233acf81e02917f13825a85bfee6e1afb32a285ca176b8f66da8db8');

/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
