# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28)
# Database: slim
# Generation Time: 2016-10-28 00:17:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dato`;

CREATE TABLE `dato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `cedula` int(11) NOT NULL,
  `natal` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dato_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_dato_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dato` WRITE;
/*!40000 ALTER TABLE `dato` DISABLE KEYS */;

INSERT INTO `dato` (`id`, `nombre`, `apellido`, `descripcion`, `cedula`, `natal`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(3,'juan','atilano','muy bien',2147483647,'torreon',90,'2016-10-24 23:08:12','2016-10-24 23:08:12'),
	(4,'z','fernandez','holas',2147483647,'one',91,'2016-10-24 23:08:12','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `dato` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table direccion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `direccion`;

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` varchar(30) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_direccion_registro_idx` (`registro_id`),
  CONSTRAINT `fk_direccion_registro` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;

INSERT INTO `direccion` (`id`, `calle`, `numero`, `colonia`, `ciudad`, `estado`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(83,'chida','0','','','',90,'2016-10-25 01:45:57','2016-10-25 01:45:57'),
	(84,'chida','0','lomas','lejos','cerca',90,'2016-10-25 01:47:48','2016-10-25 01:47:48'),
	(85,'chida','0','lomas','lejos','cerca',90,'2016-10-25 01:53:50','2016-10-25 01:53:50');

/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table educacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `educacion`;

CREATE TABLE `educacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `especialidad` varchar(100) NOT NULL DEFAULT '',
  `escuela` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `inicio` varchar(30) NOT NULL,
  `termino` varchar(30) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_educacion_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_educacion_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;

INSERT INTO `educacion` (`id`, `especialidad`, `escuela`, `ciudad`, `estado`, `inicio`, `termino`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(1,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:12:20','2016-10-25 01:12:20'),
	(2,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:07','2016-10-25 01:14:07'),
	(3,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(4,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(5,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(8,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(9,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(10,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:10','2016-10-25 01:14:10');

/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table horarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `horarios`;

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(45) NOT NULL,
  `inicio` varchar(100) NOT NULL,
  `cierre` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `direccion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_horarios_direccion1_idx` (`direccion_id`),
  CONSTRAINT `fk_horarios_direccion1` FOREIGN KEY (`direccion_id`) REFERENCES `direccion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;

INSERT INTO `horarios` (`id`, `dia`, `inicio`, `cierre`, `updated_at`, `created_at`, `direccion_id`)
VALUES
	(2,'lunes','9:30','3:90','2016-10-25 17:28:18','2016-10-25 17:28:18',83);

/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mails`;

CREATE TABLE `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mails_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_mails_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mails` WRITE;
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;

INSERT INTO `mails` (`id`, `mail`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(2,'juan',3,'2016-10-25 00:22:18','2016-10-25 00:22:18'),
	(3,'juanh',3,'2016-10-26 23:29:01','2016-10-26 23:29:01'),
	(4,'juanh',3,'2016-10-26 23:31:44','2016-10-26 23:31:44');

/*!40000 ALTER TABLE `mails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operaciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operaciones`;

CREATE TABLE `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operaciones_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_operaciones_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;

INSERT INTO `operaciones` (`id`, `tag`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'dededd',3,'2016-10-25 00:47:35','2016-10-25 00:47:35'),
	(2,'dededd',3,'2016-10-25 00:47:41','2016-10-25 00:47:41'),
	(3,'dededd',3,'2016-10-25 00:53:42','2016-10-25 00:53:42'),
	(4,'dededd',3,'2016-10-25 00:53:46','2016-10-25 00:53:46'),
	(5,'dededd',3,'2016-10-25 00:53:47','2016-10-25 00:53:47'),
	(7,'dededd',3,'2016-10-25 00:53:48','2016-10-25 00:53:48'),
	(8,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49'),
	(9,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49');

/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registro`;

CREATE TABLE `registro` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL DEFAULT '',
  `membresia` varchar(20) NOT NULL DEFAULT 'inactivo',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `token` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_UNIQUE` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;

INSERT INTO `registro` (`id`, `user`, `password`, `membresia`, `updated_at`, `created_at`, `token`)
VALUES
	(77,'admin','$2y$10$wTp0BenVZo1tllwccv6elOAsryyJg5KrzNyIf47y0ElMU/3lj2Hku','admin','2016-10-26 23:47:53','2016-10-19 20:37:26','01e49c5a651664ede2a64bca2523831ea8aa4dd4e1de76aa8349ff7c290047b9'),
	(90,'premium','$2y$10$zEzLJQSHgJaPUIYSN8W/fukQ1F7vDW7vB1X3wwo9pKXTqLv9DdrSG','premium','2016-10-24 21:15:00','2016-10-24 20:55:23','d7e488e61126176201a2a7b0a4a561eb584e6567b9cf7d82d9c257ab6c7cea4f'),
	(91,'regular','$2y$10$pWz3fpDKrW3yhvAQ59T69.Pu4zcNEYttK6ASZmdn.ajK8u7joAbCK','regular','2016-10-27 00:39:12','2016-10-24 20:56:26','9691011c837a327b344c018481b2eb194b5198c93cc76ea3bfd94d042ec90ec5'),
	(92,'inactivo','$2y$10$DNS9rQk3NqX1Ds267FY0Ves7GLA6vFOjP81vD9ComHupkLGJka7P.','inactivo','2016-10-24 20:58:39','2016-10-24 20:58:39',NULL);

/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table telefonos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telefonos`;

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` varchar(45) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_telefonos_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_telefonos_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;

INSERT INTO `telefonos` (`id`, `info`, `numero`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'home','98239823',3,'2016-10-24 23:33:15','2016-10-24 23:33:15'),
	(2,'home','98239823',3,'2016-10-24 23:33:26','2016-10-24 23:33:26');

/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
