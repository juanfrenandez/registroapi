# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28)
# Database: slim
# Generation Time: 2016-11-01 02:03:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dato`;

CREATE TABLE `dato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `cedula` int(11) NOT NULL,
  `natal` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dato_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_dato_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dato` WRITE;
/*!40000 ALTER TABLE `dato` DISABLE KEYS */;

INSERT INTO `dato` (`id`, `nombre`, `apellido`, `descripcion`, `titulo`, `cedula`, `natal`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(3,'juan','atilano','muy bien','',2147483647,'torreon',90,'2016-10-24 23:08:12','2016-10-24 23:08:12'),
	(4,'z','fernandez','holas','',2147483647,'one',91,'2016-10-24 23:08:12','0000-00-00 00:00:00'),
	(5,'ewdwed','dwed','dwed','nuevo',53454353,'d2323',77,'2016-10-31 05:48:05','2016-10-31 05:48:05'),
	(6,'f34f43','f34f3','f34f34f','f34f',0,'f34f3',96,'2016-10-31 19:55:56','2016-10-31 19:55:56'),
	(7,'dwed','dwedwe','dwed','dwedwedwed',3423432,'erteter',98,'2016-10-31 19:59:04','2016-10-31 19:59:04'),
	(8,'ponchito','orles','nuevo','ninguna',939393,'js',100,'2016-10-31 20:01:50','2016-10-31 20:01:50'),
	(9,'dwed','dwedwe','dwed','dwed',232,'wedwed',101,'2016-10-31 22:02:39','2016-10-31 22:02:39'),
	(10,'jake','jake','jake','jake',545454545,'jake',102,'2016-11-01 00:30:53','2016-11-01 00:30:53');

/*!40000 ALTER TABLE `dato` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table direccion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `direccion`;

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` varchar(30) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_direccion_registro_idx` (`registro_id`),
  CONSTRAINT `fk_direccion_registro` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;

INSERT INTO `direccion` (`id`, `calle`, `numero`, `colonia`, `ciudad`, `estado`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(83,'chida','0','','','',90,'2016-10-25 01:45:57','2016-10-25 01:45:57'),
	(84,'chida','0','lomas','lejos','cerca',90,'2016-10-25 01:47:48','2016-10-25 01:47:48'),
	(85,'chida','0','lomas','lejos','cerca',90,'2016-10-25 01:53:50','2016-10-25 01:53:50'),
	(86,'dwedew','333','dwed23','d2323d','jn6jh6767',77,'2016-10-31 06:54:56','2016-10-31 06:54:56'),
	(87,'grgrgg','5345','vfvd','r4r4rr','r4r4',77,'2016-10-31 06:54:56','0000-00-00 00:00:00'),
	(88,'nueva','34','nsdl','dknslfsn','fnwlef',77,'2016-10-31 20:29:24','2016-10-31 20:29:24'),
	(89,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:13','2016-10-31 20:30:13'),
	(90,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:35','2016-10-31 20:30:35'),
	(91,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:37','2016-10-31 20:30:37'),
	(92,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:38','2016-10-31 20:30:38'),
	(93,'lopez','34','mateos','torreon','edo',77,'2016-10-31 20:40:56','2016-10-31 20:40:56'),
	(94,'de','23233','dwed','dwedewd','dewd',77,'2016-10-31 20:44:11','2016-10-31 20:44:11'),
	(95,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:44:57','2016-10-31 20:44:57'),
	(96,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:45:42','2016-10-31 20:45:42'),
	(97,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:45:55','2016-10-31 20:45:55'),
	(98,'sdsa','vmv','mv','mvmvmn','mvmnvvm',77,'2016-10-31 20:46:47','2016-10-31 20:46:47'),
	(99,'sdsa','vmv','mv','mvmvmn','mvmnvvm',77,'2016-10-31 20:48:17','2016-10-31 20:48:17'),
	(100,'dsssdsdf','232','dwedw','dwed','dwedwed',77,'2016-10-31 20:49:12','2016-10-31 20:49:12'),
	(101,'nbdwke','dnqlndlwed','dnweldnew','dnldnlwe','ndlwedn',98,'2016-10-31 20:50:24','2016-10-31 20:50:24'),
	(102,'jake','jake','jake','jake','jake',102,'2016-11-01 00:31:55','2016-11-01 00:31:55');

/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table educacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `educacion`;

CREATE TABLE `educacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrera` varchar(100) NOT NULL DEFAULT '',
  `escuela` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `inicio` varchar(30) NOT NULL,
  `termino` varchar(30) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_educacion_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_educacion_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;

INSERT INTO `educacion` (`id`, `carrera`, `escuela`, `ciudad`, `estado`, `inicio`, `termino`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(1,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:12:20','2016-10-25 01:12:20'),
	(2,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:07','2016-10-25 01:14:07'),
	(3,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(4,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(5,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(8,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(9,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(10,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:10','2016-10-25 01:14:10'),
	(11,'f44f54f54','fg4f45','d23d23d','d3d332','dede3','dede',91,'2016-10-25 01:14:14','2016-10-25 01:14:13'),
	(12,'dwedewd','dwedewd','dwedewd','wdwed','dwedw','dwedewd',77,'2016-10-31 06:49:49','2016-10-31 06:49:49'),
	(13,'unam','unam','cdmx','df','10','12',98,'2016-10-31 20:23:09','2016-10-31 20:23:09'),
	(14,'jake','jake','jake','jake','jake','jake',102,'2016-11-01 00:31:25','2016-11-01 00:31:25');

/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table horarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `horarios`;

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(45) NOT NULL,
  `inicio` varchar(100) NOT NULL,
  `cierre` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `direccion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_horarios_direccion1_idx` (`direccion_id`),
  CONSTRAINT `fk_horarios_direccion1` FOREIGN KEY (`direccion_id`) REFERENCES `direccion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;

INSERT INTO `horarios` (`id`, `dia`, `inicio`, `cierre`, `updated_at`, `created_at`, `direccion_id`)
VALUES
	(2,'lunes','9:30','3:90','2016-10-25 17:28:18','2016-10-25 17:28:18',83),
	(3,'egg3','343','3434','2016-10-31 07:05:36','2016-10-31 07:05:36',86),
	(4,'d3d3','dw3d','dw3d','2016-10-31 07:34:00','2016-10-31 07:34:00',87),
	(5,'dwed','dewdwe','dwedd','2016-10-31 21:09:01','2016-10-31 21:09:01',86),
	(6,'lunes','8','12','2016-11-01 00:33:35','2016-11-01 00:33:35',102);

/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mails`;

CREATE TABLE `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mails_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_mails_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mails` WRITE;
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;

INSERT INTO `mails` (`id`, `mail`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(2,'juan',3,'2016-10-25 00:22:18','2016-10-25 00:22:18'),
	(3,'juanh',3,'2016-10-26 23:29:01','2016-10-26 23:29:01'),
	(4,'juanh',3,'2016-10-26 23:31:44','2016-10-26 23:31:44'),
	(5,'deddewd',5,'2016-10-31 06:12:57','2016-10-31 06:12:57'),
	(6,'juan',7,'2016-10-31 21:25:40','2016-10-31 21:25:40'),
	(7,'juan',7,'2016-10-31 21:26:12','2016-10-31 21:26:12'),
	(8,'juan',7,'2016-10-31 21:26:13','2016-10-31 21:26:13'),
	(9,'jake@jale.com',10,'2016-11-01 00:33:49','2016-11-01 00:33:49'),
	(10,'juan',10,'2016-11-01 00:37:44','2016-11-01 00:37:44'),
	(11,'juan',10,'2016-11-01 00:37:48','2016-11-01 00:37:48');

/*!40000 ALTER TABLE `mails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operaciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operaciones`;

CREATE TABLE `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operaciones_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_operaciones_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;

INSERT INTO `operaciones` (`id`, `tag`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'dededd',3,'2016-10-25 00:47:35','2016-10-25 00:47:35'),
	(2,'dededd',3,'2016-10-25 00:47:41','2016-10-25 00:47:41'),
	(3,'dededd',3,'2016-10-25 00:53:42','2016-10-25 00:53:42'),
	(4,'dededd',3,'2016-10-25 00:53:46','2016-10-25 00:53:46'),
	(5,'dededd',3,'2016-10-25 00:53:47','2016-10-25 00:53:47'),
	(7,'dededd',3,'2016-10-25 00:53:48','2016-10-25 00:53:48'),
	(8,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49'),
	(9,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49'),
	(10,'nuevo',5,'2016-10-31 06:31:36','2016-10-31 06:31:36'),
	(11,'lumbar',7,'2016-10-31 21:52:07','2016-10-31 21:52:07'),
	(12,'transplante de mama',10,'2016-11-01 00:34:27','2016-11-01 00:34:27');

/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registro`;

CREATE TABLE `registro` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL DEFAULT '',
  `membresia` varchar(20) NOT NULL DEFAULT 'inactivo',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `token` varchar(4000) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_UNIQUE` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;

INSERT INTO `registro` (`id`, `user`, `password`, `membresia`, `updated_at`, `created_at`, `token`, `deleted_at`)
VALUES
	(77,'admin','$2y$10$wTp0BenVZo1tllwccv6elOAsryyJg5KrzNyIf47y0ElMU/3lj2Hku','admin','2016-11-01 01:02:29','2016-10-19 20:37:26','08b0ccf66777d2ca395f173d49669863bfffb6064f0ae3d0618024d7821f142e',NULL),
	(90,'premium','$2y$10$zEzLJQSHgJaPUIYSN8W/fukQ1F7vDW7vB1X3wwo9pKXTqLv9DdrSG','premium','2016-10-29 03:04:06','2016-10-24 20:55:23','76433f590c1f07eb1db7a7b8005091f832f49f7c09dd7bf3254990ff041339ae',NULL),
	(91,'regular','$2y$10$pWz3fpDKrW3yhvAQ59T69.Pu4zcNEYttK6ASZmdn.ajK8u7joAbCK','regular','2016-10-28 18:48:51','2016-10-24 20:56:26','9691011c837a327b344c018481b2eb194b5198c93cc76ea3bfd94d042ec90ec5',NULL),
	(92,'inactivo','$2y$10$DNS9rQk3NqX1Ds267FY0Ves7GLA6vFOjP81vD9ComHupkLGJka7P.','inactivo','2016-10-24 20:58:39','2016-10-24 20:58:39',NULL,NULL),
	(94,'javier','$2y$10$rhbuTMKVT5ZJslK0QY0wAOISXsrX6I352lUb6na0aeZaaDATQ./ni','regular','2016-11-01 00:24:12','2016-10-29 00:35:48',NULL,'2016-11-01 00:24:12'),
	(95,'jose','$2y$10$97spk.haPu8MHGiD82iESOeXQPGHiqCaolUjoDGe4U6nCIoj9toYC','premium','2016-11-01 00:23:56','2016-10-29 00:41:21','1ef2d1dd2bade1f0fdd4e7d4d132355b45102e8422c38a676bea12c69310f6c7',NULL),
	(96,'prueba','$2y$10$.BNYBc7XGen0wqkWUrUShuzY7EzKNPg6K/XCe5acJqWoDmEcBSEE.','regular','2016-11-01 00:24:53','2016-10-29 02:31:18',NULL,NULL),
	(98,'juan','$2y$10$DmmOxbJSI35Io9UT/c/1keNk9YiKukZe8uMP6T0PfRnpwqN2UZ.De','premium','2016-10-31 18:38:35','2016-10-31 18:38:35',NULL,NULL),
	(99,'leonardo dicaprio','$2y$10$76jkC92c4wByyspXAJrCMO2G1ZfdcZuCcWBR8HOKn2yRM2fIFIouy','regular','2016-10-31 18:46:12','2016-10-31 18:46:12',NULL,NULL),
	(100,'poncho','$2y$10$G7mcwaS1bxdRX.MKdX2P..4kLbQ3UZEyElqRfpoFuG5Gh/rgJFZaW','premium','2016-11-01 00:24:24','2016-10-31 18:49:34',NULL,'2016-11-01 00:24:24'),
	(101,'nicolai','$2y$10$s/M3vk78l.RkpllKfcJWK.1vYCebAe2n200N/ZiG7y31Y2sB7xR3O','premium','2016-10-31 21:55:24','2016-10-31 21:53:18','907e4b88e12ba72921c02199355dbd1a114b5387e6b882283a50dad58fd17ca8',NULL),
	(102,'jake','$2y$10$K5ZVMHxATNkCe/NlZsbK6Oz63OfEE6YopTtLib8aMn0Ev5fRM8Jkq','premium','2016-11-01 00:35:48','2016-11-01 00:29:54','5bfccbbfbfd6ed44eac0c393dce7d4a0d211280b327a4b8fd9a24cce9fb5c264',NULL);

/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table telefonos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telefonos`;

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` varchar(45) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_telefonos_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_telefonos_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;

INSERT INTO `telefonos` (`id`, `info`, `numero`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'home','98239823',3,'2016-10-24 23:33:15','2016-10-24 23:33:15'),
	(2,'home','98239823',3,'2016-10-24 23:33:26','2016-10-24 23:33:26'),
	(4,'whatsapp','32423432322423432',3,'2016-10-29 00:44:59','2016-10-29 00:44:59'),
	(5,'fsdfsd','4234342',5,'2016-10-31 06:27:11','2016-10-31 06:27:11'),
	(6,'fsdfsd','4234342',5,'2016-10-31 06:27:23','2016-10-31 06:27:23'),
	(7,'fsdfsd','4234342',5,'2016-10-31 06:27:24','2016-10-31 06:27:24'),
	(8,'whatsapp','93932923',7,'2016-10-31 21:38:47','2016-10-31 21:38:47'),
	(9,'whatsapp','932932932',7,'2016-10-31 21:42:38','2016-10-31 21:42:38');

/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
