# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28)
# Database: slim
# Generation Time: 2016-11-30 00:41:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dato`;

CREATE TABLE `dato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `mail` varchar(80) NOT NULL,
  `descripcion` text NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `cedula` int(11) NOT NULL,
  `natal` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `image` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dato_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_dato_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dato` WRITE;
/*!40000 ALTER TABLE `dato` DISABLE KEYS */;

INSERT INTO `dato` (`id`, `nombre`, `apellido`, `mail`, `descripcion`, `titulo`, `cedula`, `natal`, `registro_id`, `updated_at`, `created_at`, `image`)
VALUES
	(3,'juan jose','atilano','','muy bien','trabajr',2147483647,'torreon',90,'2016-11-03 01:02:51','2016-10-24 23:08:12',NULL),
	(4,'z','fernandez','','holas','',2147483647,'one',91,'2016-10-24 23:08:12','0000-00-00 00:00:00',NULL),
	(5,'hola','atilano','','muy bien','trabajar',2147483647,'torreon',77,'2016-11-05 01:38:55','2016-10-31 05:48:05','http://localhost:8888/pruebasapiAmcper/archivos/1-a.jpg'),
	(6,'f34f43','f34f3','','f34f34f','f34f',0,'f34f3',96,'2016-10-31 19:55:56','2016-10-31 19:55:56',NULL),
	(7,'dwed','dwedwe','','dwed','dwedwedwed',3423432,'erteter',98,'2016-10-31 19:59:04','2016-10-31 19:59:04',NULL),
	(8,'ponchito','orles','','nuevo','ninguna',939393,'js',100,'2016-10-31 20:01:50','2016-10-31 20:01:50',NULL),
	(9,'dwed','dwedwe','','dwed','dwed',232,'wedwed',101,'2016-10-31 22:02:39','2016-10-31 22:02:39',NULL),
	(10,'jake','jake','','jake','jake',545454545,'jake',102,'2016-11-01 00:30:53','2016-11-01 00:30:53',NULL),
	(11,'juan','fernandez','','alguna','alguna',2147483647,'torreon',103,'2016-11-02 00:57:16','2016-11-02 00:57:16',NULL),
	(12,'fsdf','juaafsdfds','','scnsdnfc','sdlnfldsnf',0,'sefesfs',92,'2016-11-03 01:05:32','2016-11-03 01:05:32',NULL),
	(14,'dewdwe','dewdew','','dd23d','23',2323,'dwedewd',104,'2016-11-03 01:43:13','2016-11-03 01:43:13',NULL),
	(15,'dsada','onhohj','','ohnoihioh','hoihoihioh',423423423,'nasflsdnld',105,'2016-11-03 01:44:47','2016-11-03 01:44:47',NULL),
	(16,'ferferf','ferferfer','','ferferferf','ferferf',534543,'dwedwed',95,'2016-11-03 01:45:42','2016-11-03 01:45:42',NULL),
	(17,'dwd','dwedd','','dwed','dwedwed',4234,'sdfff',106,'2016-11-03 17:19:21','2016-11-03 17:19:21',NULL),
	(18,'lensillo','ollisnoel','','leocontitulo','leodoctor',2147483647,'leocity',99,'2016-11-03 21:03:00','2016-11-03 21:03:00',NULL),
	(19,'final','apefinal','','desfinal','espfinal',20302221,'ciudadfinal',109,'2016-11-03 22:20:54','2016-11-03 22:20:54',NULL),
	(20,'listo','listo','','listo','listo',0,'laita',110,'2016-11-03 22:58:55','2016-11-03 22:58:55',NULL),
	(21,'bonafont','bona','','font','alguna',0,'fontbona',111,'2016-11-05 01:00:11','2016-11-05 01:00:11',NULL),
	(22,'cdcd','cdcdc','','cdcd','cdcdc',0,'ededed',118,'2016-11-09 22:41:32','2016-11-09 22:41:32',NULL),
	(23,'alguno','alguno','','alguna','alguna',0,'alguna',123,'2016-11-10 00:37:35','2016-11-10 00:37:35',NULL),
	(24,'pancho','pancho','','pancho','pancho',0,'pancho',124,'2016-11-10 00:57:59','2016-11-10 00:57:59',NULL),
	(25,'default','default','','default','default',49028234,' ',135,'2016-11-29 23:49:01','2016-11-29 23:49:01',NULL),
	(26,'default','default','ferrari@ferrari.com.es','default','default',324234,' ',136,'2016-11-30 00:26:26','2016-11-30 00:26:26',NULL);

/*!40000 ALTER TABLE `dato` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table direccion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `direccion`;

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` varchar(30) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_direccion_registro_idx` (`registro_id`),
  CONSTRAINT `fk_direccion_registro` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;

INSERT INTO `direccion` (`id`, `calle`, `numero`, `colonia`, `ciudad`, `estado`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(83,'chida','0','','','',90,'2016-10-25 01:45:57','2016-10-25 01:45:57'),
	(84,'chida2','0','lomas','lejos','cerca',90,'2016-11-05 01:08:45','2016-10-25 01:47:48'),
	(85,'chida','0','lomas','lejos','cerca',90,'2016-10-25 01:53:50','2016-10-25 01:53:50'),
	(86,'Direcciones','333','dwed23','d2323d','jn6jh6767',77,'2016-11-05 01:27:00','2016-10-31 06:54:56'),
	(87,'grgrgg','5345','vfvd','r4r4rr','r4r4',77,'2016-10-31 06:54:56','0000-00-00 00:00:00'),
	(88,'nueva','34','nsdl','dknslfsn','fnwlef',77,'2016-10-31 20:29:24','2016-10-31 20:29:24'),
	(89,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:13','2016-10-31 20:30:13'),
	(90,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:35','2016-10-31 20:30:35'),
	(91,'nueva','383','eugenio','lerdo','durango',77,'2016-10-31 20:30:37','2016-10-31 20:30:37'),
	(92,'nuevacolor nuevo','383','eugenio','lerdo','durango',77,'2016-11-01 19:43:42','2016-10-31 20:30:38'),
	(93,'lopez','34','mateos','torreon','edo',77,'2016-10-31 20:40:56','2016-10-31 20:40:56'),
	(94,'de','23233','dwed','dwedewd','dewd',77,'2016-10-31 20:44:11','2016-10-31 20:44:11'),
	(95,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:44:57','2016-10-31 20:44:57'),
	(96,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:45:42','2016-10-31 20:45:42'),
	(97,'de','89','xasbxa','xbaxjabk','nk',77,'2016-10-31 20:45:55','2016-10-31 20:45:55'),
	(98,'sdsa','vmv','mv','mvmvmn','mvmnvvm',77,'2016-10-31 20:46:47','2016-10-31 20:46:47'),
	(99,'sdsa','vmv','mv','mvmvmn','mvmnvvm',77,'2016-10-31 20:48:17','2016-10-31 20:48:17'),
	(100,'dsssdsdf','232','dwedw','dwed','dwedwed',77,'2016-10-31 20:49:12','2016-10-31 20:49:12'),
	(101,'nbdwke','dnqlndlwed','dnweldnew','dnldnlwe','ndlwedn',98,'2016-10-31 20:50:24','2016-10-31 20:50:24'),
	(102,'jake','jake','jake','jake','jake',102,'2016-11-01 00:31:55','2016-11-01 00:31:55'),
	(103,'siempreviva','93','nueva','chida','alguno',103,'2016-11-02 20:20:27','2016-11-02 20:20:27'),
	(104,'alguna','393','dejnde','dwdw','ofofof',103,'2016-11-02 20:48:33','2016-11-02 20:48:33'),
	(105,'juan','43','fdsfdsf','fsdfsdf','fsdfsdf',103,'2016-11-02 22:36:45','2016-11-02 22:36:45'),
	(106,'alguna','alguna','finalalguna','finalalguna','algunfinal',109,'2016-11-03 22:29:23','2016-11-03 22:29:23'),
	(107,'direccion','asda','ndosndoq','nfodqoqd','ndoasndao',96,'2016-11-03 22:48:05','2016-11-03 22:48:05'),
	(108,'dnasd','nasdlna','asdnaskdnl','asndald','andla',110,'2016-11-03 22:59:16','2016-11-03 22:59:16'),
	(109,'cotilla','23423234','vallarta','gdl','jal',111,'2016-11-05 01:16:33','2016-11-05 01:16:33'),
	(110,'f34ff','343','f34f4f','f34f3f','f34f3f',118,'2016-11-09 22:41:56','2016-11-09 22:41:56'),
	(111,'direccion','direccion','direccion','direccion','direccion',123,'2016-11-10 00:38:37','2016-11-10 00:38:37'),
	(112,'23','23','23','23','23',123,'2016-11-10 00:40:27','2016-11-10 00:40:27'),
	(113,'cv','87','bv','bv','bv',123,'2016-11-10 00:40:52','2016-11-10 00:40:52'),
	(114,'calle','calle','calle ','calle','calle',124,'2016-11-10 00:58:33','2016-11-10 00:58:33'),
	(115,'chida','chida','chida','chiad','chida',124,'2016-11-10 00:59:10','2016-11-10 00:59:10'),
	(116,'chida','chida','chida','chiad','chida',124,'2016-11-10 00:59:11','2016-11-10 00:59:11'),
	(117,'chida','chida','chida','chiad','chida',124,'2016-11-10 00:59:13','2016-11-10 00:59:13'),
	(118,'fsdfsd','fsdfs','sdfsdf','fsdfds','sdfsdf',77,'2016-11-10 17:07:36','2016-11-10 17:07:36');

/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table educacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `educacion`;

CREATE TABLE `educacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrera` varchar(100) NOT NULL DEFAULT '',
  `escuela` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `inicio` varchar(30) NOT NULL,
  `termino` varchar(30) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_educacion_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_educacion_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;

INSERT INTO `educacion` (`id`, `carrera`, `escuela`, `ciudad`, `estado`, `inicio`, `termino`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(1,'deedede','lnfsdlf','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-11-07 20:47:49','2016-10-25 01:12:20'),
	(2,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:07','2016-10-25 01:14:07'),
	(3,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(4,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(5,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:08','2016-10-25 01:14:08'),
	(8,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(9,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:09','2016-10-25 01:14:09'),
	(10,'deedede','nuevoescuela','villa','lejos','Enero de 1990','Febrero de 19920',90,'2016-10-25 01:14:10','2016-10-25 01:14:10'),
	(11,'f44f54f54','fg4f45','d23d23dx','d3d332','dede3','dede',91,'2016-11-05 01:28:27','2016-10-25 01:14:13'),
	(12,'f44f54f5434','fg4f45','d23d23d','d3d332','dede3','dede',77,'2016-11-05 01:26:50','2016-10-31 06:49:49'),
	(13,'unam','unam','cdmx','df','10','12',98,'2016-10-31 20:23:09','2016-10-31 20:23:09'),
	(14,'jake','jake','jake','jake','jake','jake',102,'2016-11-01 00:31:25','2016-11-01 00:31:25'),
	(15,'Otro registro homms','Nueva','Nueva','Todo nuevo','hoy','mañana',77,'2016-11-05 01:10:09','2016-11-01 18:50:55'),
	(16,'Otro registro carai2','Nueva','Nueva','Todo nuevo','hoy','mañana',77,'2016-11-10 17:09:48','2016-11-01 18:51:03'),
	(17,'Medicina','UDG','GDL','Jalisco','20','25',103,'2016-11-02 16:42:58','2016-11-02 16:42:58'),
	(18,'final','final','final','final','final','final',109,'2016-11-03 22:21:42','2016-11-03 22:21:42'),
	(19,'lisaio','ndla','asdnsad','andsal','asdnas','asdasda',110,'2016-11-03 22:59:07','2016-11-03 22:59:07'),
	(20,'doctor','udg','gdl','jal','1922','2977',90,'2016-11-05 00:26:59','2016-11-05 00:26:59'),
	(21,'unidep','unip','alguna','talvez','23','944',111,'2016-11-05 01:11:19','2016-11-05 01:11:19'),
	(22,'unidep','unip','alguna','talvez','23','944',111,'2016-11-05 01:15:35','2016-11-05 01:15:35'),
	(23,'UDG','UDG','Tepic','Nayarit','2005','2010',112,'2016-11-07 20:42:17','2016-11-07 20:42:17'),
	(24,'dededed','eefwerfwe','erfewrf','ferfewrf','ferfer','Fwefewf',118,'2016-11-09 22:41:44','2016-11-09 22:41:44'),
	(25,'<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>',118,'2016-11-09 23:01:18','2016-11-09 23:01:18'),
	(26,'estudios','estudios','estudios','estudios','estudios','estudios',123,'2016-11-10 00:37:58','2016-11-10 00:37:58'),
	(27,'escuela','esuclea','escuela','escuela','escuela','escuela',124,'2016-11-10 00:58:15','2016-11-10 00:58:15'),
	(28,'escuela','esuclea','escuela','escuela','escuela','escuela',124,'2016-11-10 00:58:16','2016-11-10 00:58:16'),
	(29,'escuela','esuclea','escuela','escuela','escuela','escuela',124,'2016-11-10 00:58:17','2016-11-10 00:58:17'),
	(30,'algunos','alguna','alguna','sksksk','asdka','asda2',77,'2016-11-10 17:07:27','2016-11-10 17:07:27'),
	(31,'<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>','<script>alert(\'123\');</script>',99,'2016-11-25 19:52:53','2016-11-25 19:52:53');

/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table horarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `horarios`;

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(45) NOT NULL,
  `inicio` varchar(100) NOT NULL,
  `cierre` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `direccion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_horarios_direccion1_idx` (`direccion_id`),
  CONSTRAINT `fk_horarios_direccion1` FOREIGN KEY (`direccion_id`) REFERENCES `direccion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;

INSERT INTO `horarios` (`id`, `dia`, `inicio`, `cierre`, `updated_at`, `created_at`, `direccion_id`)
VALUES
	(2,'lunes','9:30','3:90','2016-10-25 17:28:18','2016-10-25 17:28:18',83),
	(3,'egg34','343','3434','2016-11-05 01:09:18','2016-10-31 07:05:36',86),
	(4,'d3d3','dw3d','dw3d','2016-10-31 07:34:00','2016-10-31 07:34:00',87),
	(5,'lunese','dewdwe','dwedd','2016-11-10 01:22:22','2016-10-31 21:09:01',86),
	(6,'lunes','8','12','2016-11-01 00:33:35','2016-11-01 00:33:35',102),
	(7,'martes','23','24','2016-11-02 21:15:13','2016-11-02 20:46:34',103),
	(8,'mirecoles','23','24','2016-11-02 21:15:30','2016-11-02 20:47:02',103),
	(9,'vdfvdf','32','23','2016-11-03 23:00:03','2016-11-03 23:00:03',108),
	(10,'lunes','4','5','2016-11-05 00:32:19','2016-11-05 00:32:19',84),
	(11,'lunas','30','303','2016-11-05 01:17:03','2016-11-05 01:17:03',109),
	(12,'23','23','23','2016-11-09 22:42:31','2016-11-09 22:42:31',110),
	(13,'lunes','3','19','2016-11-10 00:39:08','2016-11-10 00:39:08',111),
	(14,'Lunes','88','87','2016-11-10 00:41:27','2016-11-10 00:41:27',113),
	(15,'martes','2','003c','2016-11-10 01:38:45','2016-11-10 00:59:40',116),
	(16,'fsdfsdf','wfweff','fsdfsdfdsdss','2016-11-10 17:07:44','2016-11-10 17:07:44',90);

/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mails`;

CREATE TABLE `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mails_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_mails_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mails` WRITE;
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;

INSERT INTO `mails` (`id`, `mail`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(2,'juan',3,'2016-10-25 00:22:18','2016-10-25 00:22:18'),
	(3,'juanh',3,'2016-10-26 23:29:01','2016-10-26 23:29:01'),
	(4,'juan@juan.com',3,'2016-11-05 00:28:35','2016-10-26 23:31:44'),
	(5,'mailchido@hola.com',5,'2016-11-01 23:35:04','2016-10-31 06:12:57'),
	(6,'juan',7,'2016-10-31 21:25:40','2016-10-31 21:25:40'),
	(7,'juan',7,'2016-10-31 21:26:12','2016-10-31 21:26:12'),
	(8,'juan',7,'2016-10-31 21:26:13','2016-10-31 21:26:13'),
	(9,'jake@jale.com',10,'2016-11-01 00:33:49','2016-11-01 00:33:49'),
	(10,'juan',10,'2016-11-01 00:37:44','2016-11-01 00:37:44'),
	(11,'juan',10,'2016-11-01 00:37:48','2016-11-01 00:37:48'),
	(12,'nuevomail4',5,'2016-11-05 01:09:24','2016-11-01 23:28:22'),
	(13,'juan@juan.com',11,'2016-11-02 20:38:05','2016-11-02 20:38:05'),
	(14,'algunomail',5,'2016-11-03 19:04:48','2016-11-03 19:04:48'),
	(15,'fwefe',20,'2016-11-03 23:00:06','2016-11-03 23:00:06'),
	(16,'juan',21,'2016-11-05 01:17:25','2016-11-05 01:17:25'),
	(17,'e32e',22,'2016-11-09 22:42:35','2016-11-09 22:42:35'),
	(18,'frefe',22,'2016-11-09 22:43:03','2016-11-09 22:43:03'),
	(19,'frefefrfrfr',22,'2016-11-09 22:43:05','2016-11-09 22:43:05'),
	(20,'asa@asa.com',23,'2016-11-10 00:39:22','2016-11-10 00:39:22'),
	(21,'alt@alt.com',23,'2016-11-10 00:41:42','2016-11-10 00:41:42'),
	(22,'tr@tr.com',23,'2016-11-10 00:41:58','2016-11-10 00:41:58'),
	(23,'pancho@mail.com.mx',24,'2016-11-10 01:31:08','2016-11-10 00:59:55');

/*!40000 ALTER TABLE `mails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operaciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operaciones`;

CREATE TABLE `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operaciones_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_operaciones_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;

INSERT INTO `operaciones` (`id`, `tag`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'dededd',3,'2016-10-25 00:47:35','2016-10-25 00:47:35'),
	(2,'dededd',3,'2016-10-25 00:47:41','2016-10-25 00:47:41'),
	(3,'dededd',3,'2016-10-25 00:53:42','2016-10-25 00:53:42'),
	(4,'dededd',3,'2016-10-25 00:53:46','2016-10-25 00:53:46'),
	(5,'dededd',3,'2016-10-25 00:53:47','2016-10-25 00:53:47'),
	(7,'dededd',3,'2016-10-25 00:53:48','2016-10-25 00:53:48'),
	(8,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49'),
	(9,'dededd',3,'2016-10-25 00:53:49','2016-10-25 00:53:49'),
	(10,'nuevo',5,'2016-10-31 06:31:36','2016-10-31 06:31:36'),
	(11,'lumbar',7,'2016-10-31 21:52:07','2016-10-31 21:52:07'),
	(12,'transplante de mama',10,'2016-11-01 00:34:27','2016-11-01 00:34:27'),
	(13,'nuevo2orales3',5,'2016-11-10 01:03:44','2016-11-02 00:15:53'),
	(14,'Implante de mama',11,'2016-11-02 20:52:43','2016-11-02 20:52:43'),
	(15,'operación',11,'2016-11-02 22:37:54','2016-11-02 20:55:43'),
	(16,'ddadasd',20,'2016-11-03 23:00:16','2016-11-03 23:00:16'),
	(17,'ddadasd',20,'2016-11-03 23:00:18','2016-11-03 23:00:18'),
	(18,'ddadasd',20,'2016-11-03 23:00:18','2016-11-03 23:00:18'),
	(19,'ddadasd',20,'2016-11-03 23:00:18','2016-11-03 23:00:18'),
	(20,'ddadasd',20,'2016-11-03 23:00:18','2016-11-03 23:00:18'),
	(21,'ddadasd',20,'2016-11-03 23:00:19','2016-11-03 23:00:19'),
	(22,'ddadasd',20,'2016-11-03 23:00:19','2016-11-03 23:00:19'),
	(23,'ddadasd',20,'2016-11-03 23:00:19','2016-11-03 23:00:19'),
	(24,'ddadasd',20,'2016-11-03 23:00:19','2016-11-03 23:00:19'),
	(25,'ddadasd',20,'2016-11-03 23:00:19','2016-11-03 23:00:19'),
	(26,'asda',21,'2016-11-05 01:24:06','2016-11-05 01:24:06'),
	(27,'dwedwe',21,'2016-11-05 01:24:10','2016-11-05 01:24:10'),
	(28,'eferref',21,'2016-11-05 01:24:13','2016-11-05 01:24:13'),
	(29,'eferrefvxcvx',21,'2016-11-05 01:24:15','2016-11-05 01:24:15'),
	(30,'yhtythyt',7,'2016-11-09 17:03:43','2016-11-09 17:03:43'),
	(31,'dwedwedwedw',22,'2016-11-09 22:42:46','2016-11-09 22:42:46'),
	(32,'dedede',23,'2016-11-10 00:39:41','2016-11-10 00:39:41'),
	(33,'dededee23e2',23,'2016-11-10 00:39:44','2016-11-10 00:39:44'),
	(34,'dededee23e2',23,'2016-11-10 00:39:44','2016-11-10 00:39:44'),
	(35,'panza',23,'2016-11-10 00:39:51','2016-11-10 00:39:51'),
	(36,'pierna',23,'2016-11-10 00:39:55','2016-11-10 00:39:55'),
	(37,'dsfsdfds',24,'2016-11-10 01:00:19','2016-11-10 01:00:19'),
	(38,'f23wefwef',24,'2016-11-10 01:21:20','2016-11-10 01:00:22'),
	(39,'dsfsdf2',5,'2016-11-10 17:10:46','2016-11-10 17:08:05'),
	(40,'dsfsdf',5,'2016-11-10 17:08:06','2016-11-10 17:08:06');

/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registro`;

CREATE TABLE `registro` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL DEFAULT '',
  `membresia` varchar(20) NOT NULL DEFAULT 'inactivo',
  `numero_cuenta` varchar(45) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `token` varchar(4000) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_UNIQUE` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;

INSERT INTO `registro` (`id`, `user`, `password`, `membresia`, `numero_cuenta`, `updated_at`, `created_at`, `token`, `deleted_at`)
VALUES
	(77,'admin','$2y$10$wTp0BenVZo1tllwccv6elOAsryyJg5KrzNyIf47y0ElMU/3lj2Hku','admin','','2016-11-07 20:46:18','2016-10-19 20:37:26','039cff0228949225d293523d1a19974d0837cba3f50b876973887f29f5c88296',NULL),
	(90,'premium','$2y$10$zEzLJQSHgJaPUIYSN8W/fukQ1F7vDW7vB1X3wwo9pKXTqLv9DdrSG','premium','','2016-11-05 00:25:27','2016-10-24 20:55:23','ac6098ef92b4549bf76dcb6d08b03bcf18a09f733164a49cdc656d627c53a3f6',NULL),
	(91,'regular','$2y$10$pWz3fpDKrW3yhvAQ59T69.Pu4zcNEYttK6ASZmdn.ajK8u7joAbCK','regular','','2016-10-28 18:48:51','2016-10-24 20:56:26','9691011c837a327b344c018481b2eb194b5198c93cc76ea3bfd94d042ec90ec5',NULL),
	(92,'inactivo','$2y$10$DNS9rQk3NqX1Ds267FY0Ves7GLA6vFOjP81vD9ComHupkLGJka7P.','inactivo','','2016-10-24 20:58:39','2016-10-24 20:58:39',NULL,NULL),
	(94,'javier','$2y$10$rhbuTMKVT5ZJslK0QY0wAOISXsrX6I352lUb6na0aeZaaDATQ./ni','regular','','2016-11-01 00:24:12','2016-10-29 00:35:48',NULL,'2016-11-01 00:24:12'),
	(95,'jose','$2y$10$97spk.haPu8MHGiD82iESOeXQPGHiqCaolUjoDGe4U6nCIoj9toYC','premium','','2016-11-01 00:23:56','2016-10-29 00:41:21','1ef2d1dd2bade1f0fdd4e7d4d132355b45102e8422c38a676bea12c69310f6c7',NULL),
	(96,'prueba','$2y$10$.BNYBc7XGen0wqkWUrUShuzY7EzKNPg6K/XCe5acJqWoDmEcBSEE.','regular','','2016-11-01 00:24:53','2016-10-29 02:31:18',NULL,NULL),
	(98,'juan','$2y$10$DmmOxbJSI35Io9UT/c/1keNk9YiKukZe8uMP6T0PfRnpwqN2UZ.De','premium','','2016-10-31 18:38:35','2016-10-31 18:38:35',NULL,NULL),
	(99,'leonardo dicaprio','$2y$10$76jkC92c4wByyspXAJrCMO2G1ZfdcZuCcWBR8HOKn2yRM2fIFIouy','regular','','2016-10-31 18:46:12','2016-10-31 18:46:12',NULL,NULL),
	(100,'poncho','$2y$10$G7mcwaS1bxdRX.MKdX2P..4kLbQ3UZEyElqRfpoFuG5Gh/rgJFZaW','premium','','2016-11-05 01:40:13','2016-10-31 18:49:34',NULL,NULL),
	(101,'nicolai','$2y$10$s/M3vk78l.RkpllKfcJWK.1vYCebAe2n200N/ZiG7y31Y2sB7xR3O','premium','','2016-10-31 21:55:24','2016-10-31 21:53:18','907e4b88e12ba72921c02199355dbd1a114b5387e6b882283a50dad58fd17ca8',NULL),
	(102,'jake','$2y$10$K5ZVMHxATNkCe/NlZsbK6Oz63OfEE6YopTtLib8aMn0Ev5fRM8Jkq','premium','','2016-11-01 00:35:48','2016-11-01 00:29:54','5bfccbbfbfd6ed44eac0c393dce7d4a0d211280b327a4b8fd9a24cce9fb5c264',NULL),
	(103,'probando','$2y$10$i412ex12a7SEls3C9zpssuRofs4l9eBotu1Hs/Pr2cxJtiye9yQuS','premium','','2016-11-02 00:18:53','2016-11-02 00:18:21','c46d261926a07e6894b6412981e72600f1fac28022b4fda659b37f386b20cf5f',NULL),
	(104,'nuevo','$2y$10$Cj3dZ.c1leXqkMvsT2G9GOxPlgnEo0.pTzYRa.Ia4pXTCLGTHK6wi','premium','','2016-11-03 01:21:26','2016-11-03 01:21:26',NULL,NULL),
	(105,'segundo','$2y$10$vhvF.BO0ZDdhQs37RqFRKOTbEkAm791DGvjUZdVw425rRztZmWLGG','premium','','2016-11-03 01:44:25','2016-11-03 01:44:25',NULL,NULL),
	(106,'tercer','$2y$10$X5VMv5ods7x3tVFRv6/8D.s6jkpGBabBcbY5sb.ZenHthoiTnWaMa','regular','','2016-11-03 17:18:53','2016-11-03 17:18:53',NULL,NULL),
	(108,'algo','$2y$10$Zp6V9o1FcBEDkR6MorMR9.lDyUzmEk5sOxQ1wKv2Fv9.Ext6fAs7K','regular','','2016-11-04 01:46:30','2016-11-03 18:22:38',NULL,NULL),
	(109,'final','$2y$10$6s2yztgc.gjgCRu1U8ISdeOvr6qtPZF9vpMLmxw0ebyLeGmUbA5EG','premium','','2016-11-03 22:20:13','2016-11-03 22:20:13',NULL,NULL),
	(110,'listo','$2y$10$C3lOZHE1aKxetYGE3b0gTeO70F4QsEaq/vE2k394PJkBvLUbDJd0u','premium','','2016-11-05 01:39:57','2016-11-03 22:58:30',NULL,'2016-11-05 01:39:57'),
	(111,'bonafont','$2y$10$fz3OggeE29ywQOfnLCY2l.k.7YJxRxlEC8s0UxFFijgnDrwhvc0mi','regular','','2016-11-05 00:30:56','2016-11-05 00:30:56',NULL,NULL),
	(112,'sarah','$2y$10$3iJiI11LLzre.mEW2o6sQ.aRYy4g2in0vxZPEBSryiJPw0ZInjuC.','premium','','2016-11-07 20:49:56','2016-11-07 20:40:53','4f553bab12243e5ca7412359519619075dd09eda6c4fc2a7f2e8478de2109a5f',NULL),
	(113,'frfrr','$2y$10$mcHqqtu9wwpY5Y9062NFr.NHFATn.5iIunnFSX74Z05/LclzSP3rq','regular','','2016-11-09 18:45:06','2016-11-09 18:45:06',NULL,NULL),
	(114,'adas','$2y$10$E3Fx.6.nvrDcVnJNBJhMkOLoU2sJihOzbmxrTvbPnSaBZlIfuuaMS','premium','','2016-11-09 19:50:03','2016-11-09 19:50:03',NULL,NULL),
	(115,'cholo','$2y$10$o1XcEa0BCZ6eSoJh/P003OlxtDRl8s3lwVFxsr7gm8JiK/zIylZR6','regular','','2016-11-09 20:24:24','2016-11-09 20:24:24',NULL,NULL),
	(116,'loco','$2y$10$OsUGvxyk4RbLqI3z70X/f.QOvnRP8SaoY9aBv/OQVyPdZKhABrI2.','inactivo','','2016-11-09 21:03:25','2016-11-09 21:03:25',NULL,NULL),
	(117,'archivo','$2y$10$h2yNFI9TJtP/97p/U5wCM.g2/b2.m697VbNsFjtW8Fb8kebO43HJ.','regular','','2016-11-09 21:04:14','2016-11-09 21:04:14',NULL,NULL),
	(118,'noviembre','$2y$10$DqqvzOpBP6YxZN67b0o/PuXPxsAv1E2NZri75jb5b1cwv7cCw3lA2','premium','','2016-11-09 21:05:05','2016-11-09 21:05:05',NULL,NULL),
	(119,'vre','$2y$10$kqHuaY8mY7ahv4jnNq.J7eyX8ItLMZxpRjCJ8/o.Q.j1VDTU8yBgK','premium','','2016-11-10 00:04:58','2016-11-10 00:04:58',NULL,NULL),
	(120,'dwd','$2y$10$xYZmvkVRt.EUTkfq76N5e.yNNrwlYU2zTKXMuBccIesrXj5McwxmW','premium','','2016-11-10 00:07:45','2016-11-10 00:07:45',NULL,NULL),
	(121,'wedwe','$2y$10$QmVUdZ.MyEDEXtSFvijI6OMwnIbF67jwLkUazq6GEn5Efoj/fGBuq','premium','','2016-11-10 00:08:55','2016-11-10 00:08:55',NULL,NULL),
	(122,'okas','$2y$10$uFoiNXqnvUDs0c0.L2NaQeChydFSkc.pXiQEJWZub3jvnvfK31dPW','premium','','2016-11-10 00:21:51','2016-11-10 00:21:51',NULL,NULL),
	(123,'casi','$2y$10$1p4yiLldYBk5D0XK5dtmX.ZXwi5/.1YQze5htGbtU59zJywV8e4he','premium','','2016-11-10 00:36:57','2016-11-10 00:36:57',NULL,NULL),
	(124,'pancho','$2y$10$ZhDip/6RF/E4XloaDVsdr.G4E7s5gVIobBtVrJjZO7Fr5Vo5KQw9i','premium','','2016-11-10 00:57:26','2016-11-10 00:57:26',NULL,NULL),
	(125,'chistoso','$2y$10$ClRzROR6zrlkQ9O.J8E3Q.52k3bl9iyWY8bneYBgHHLH7ylunoyHm','inactivo','','2016-11-25 19:48:56','2016-11-25 19:48:56',NULL,NULL),
	(126,'nkdjckjbdskjdbdfbs','$2y$10$sTrUON17BJLr.kJktLVO0uTGPToi4xBHyfC1U9yX09VPA3ND6YJgW','premium','','2016-11-25 19:50:07','2016-11-25 19:50:07',NULL,NULL),
	(127,'der','$2y$10$z.VmwEGUE2zVkyV2xv4zceBUUzhOKZ7n9SV6PhZmIY8T4OdLMQcIa','premium','','2016-11-25 19:54:42','2016-11-25 19:54:42',NULL,NULL),
	(128,'hola','$2y$10$7AXfCYc6DtWoRnz.WFEiH.K5ep6t7Fa9KnrWchm7xNhQakwz1S/sW','premium','','2016-11-29 20:47:00','2016-11-29 20:47:00',NULL,NULL),
	(129,'esteban','$2y$10$5ELIV3YrTslhJce/13At2.re3utdaAKRHvOhAC5SfecDhM5XLSAmK','premium','','2016-11-29 21:09:40','2016-11-29 21:09:40',NULL,NULL),
	(130,'2r43r','$2y$10$aEk.QhYC8wAqnn6y4Io9Ze6EaurIHHW2aN36m65XqOnIONF6selWa','premium','','2016-11-29 21:15:07','2016-11-29 21:15:07',NULL,NULL),
	(131,'vrtvrt','$2y$10$j2SMh9IsHWsgcluWin8PDeNfiO02QzSCJecA9oE9t6DpRazSsaCfG','premium','','2016-11-29 21:15:30','2016-11-29 21:15:30',NULL,NULL),
	(132,'derderew','$2y$10$fMvsMKSaxGNnIbxD4dSLwOGTEFk6lxF2/BxzOQmoVUKjLlCMWHGy2','premium','','2016-11-29 21:16:05','2016-11-29 21:16:05',NULL,NULL),
	(133,'dewd','$2y$10$dnO4bjRVROG2iYT100/it.vQ7bojnEJNx0oVE1oOJptMfj0cImP/q','premium','','2016-11-29 23:09:05','2016-11-29 23:09:05',NULL,NULL),
	(134,'Hansolo','$2y$10$KVJSQk9adofkdKUF7w3QG./KGAduCPmP0hpwGgYOksJNbgOZADHTq','premium','','2016-11-29 23:46:53','2016-11-29 23:46:53',NULL,NULL),
	(135,'hansolitario','$2y$10$P6ohvbHal56bp.UpzS/fDOk4.Hf5YYm/MHB56dIBFBZknqHjA3qYi','premium','','2016-11-29 23:49:01','2016-11-29 23:49:01',NULL,NULL),
	(136,'ferrari','$2y$10$k0SCux9Is.m735Dx0FIXuOuX5iLCeslKBZVZ2RLZo9uWgY.1s.DEK','premium','','2016-11-30 00:26:26','2016-11-30 00:26:26',NULL,NULL);

/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table telefonos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telefonos`;

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` varchar(45) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `dato_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_telefonos_dato1_idx` (`dato_id`),
  CONSTRAINT `fk_telefonos_dato1` FOREIGN KEY (`dato_id`) REFERENCES `dato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;

INSERT INTO `telefonos` (`id`, `info`, `numero`, `dato_id`, `updated_at`, `created_at`)
VALUES
	(1,'home','98239823',3,'2016-10-24 23:33:15','2016-10-24 23:33:15'),
	(2,'home','98239823',3,'2016-10-24 23:33:26','2016-10-24 23:33:26'),
	(4,'whatsapp','32423432322423432',3,'2016-10-29 00:44:59','2016-10-29 00:44:59'),
	(5,'home','090909',5,'2016-11-02 00:03:09','2016-10-31 06:27:11'),
	(6,'office','121212122a',5,'2016-11-05 01:27:54','2016-10-31 06:27:23'),
	(7,'whatsapp','8282828282',5,'2016-11-02 00:02:08','2016-10-31 06:27:24'),
	(8,'whatsapp','93932923',7,'2016-10-31 21:38:47','2016-10-31 21:38:47'),
	(9,'whatsapp','932932932',7,'2016-10-31 21:42:38','2016-10-31 21:42:38'),
	(10,'home','9023490342902',11,'2016-11-02 20:45:45','2016-11-02 20:45:45'),
	(11,'fwef','4234',20,'2016-11-03 23:00:12','2016-11-03 23:00:12'),
	(12,'casa','838383',21,'2016-11-05 01:23:44','2016-11-05 01:23:44'),
	(13,'232e23e','234234234',22,'2016-11-09 22:42:43','2016-11-09 22:42:43'),
	(14,'home','234234234',23,'2016-11-10 00:39:35','2016-11-10 00:39:35'),
	(15,'whatsapp','ffff',24,'2016-11-10 01:25:41','2016-11-10 01:00:16');

/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
