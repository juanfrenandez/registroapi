# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28)
# Database: slim
# Generation Time: 2016-10-19 23:47:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table direccion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `direccion`;

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` int(11) NOT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_direccion_registro_idx` (`registro_id`),
  CONSTRAINT `fk_direccion_registro` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;

INSERT INTO `direccion` (`id`, `calle`, `numero`, `colonia`, `ciudad`, `estado`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(2,'siempreviva',23,NULL,NULL,NULL,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(19,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(22,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(23,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(24,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(25,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(26,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(27,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(28,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(29,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(30,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(31,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(32,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(33,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(34,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(35,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(36,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(37,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(38,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(39,'siempreviva',23,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(40,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(41,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(42,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(43,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(44,'dwedwed',53,NULL,NULL,NULL,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(46,'chida',0,NULL,NULL,NULL,70,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(47,'dwedwed',4234,NULL,NULL,NULL,70,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(48,'dwedwed',4234,NULL,NULL,NULL,70,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(49,'namvre',334,NULL,NULL,NULL,69,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(50,'namvre',334,NULL,NULL,NULL,69,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(51,'namvre',334,NULL,NULL,NULL,69,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(52,'namvre',334,NULL,NULL,NULL,69,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(53,'nueva',34,NULL,NULL,NULL,74,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(54,'nueva',34,NULL,NULL,NULL,74,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(55,'nueva',34,NULL,NULL,NULL,74,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(56,'nueva',34,NULL,NULL,NULL,74,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(57,'nueva',34,NULL,NULL,NULL,75,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(58,'nuevae23e3222',3,NULL,NULL,NULL,75,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(59,'siempreviva',33,NULL,NULL,NULL,74,'2016-10-19 20:03:38','2016-10-19 20:03:38'),
	(60,'una nueva',45,NULL,NULL,NULL,77,'2016-10-20 00:30:48','2016-10-19 23:26:23');

/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table educacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `educacion`;

CREATE TABLE `educacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `especialidad` varchar(100) NOT NULL DEFAULT '',
  `escuela` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `inicio` varchar(30) NOT NULL,
  `termino` varchar(30) NOT NULL,
  `registro_id` int(6) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_educacion_registro1_idx` (`registro_id`),
  CONSTRAINT `fk_educacion_registro1` FOREIGN KEY (`registro_id`) REFERENCES `registro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;

INSERT INTO `educacion` (`id`, `especialidad`, `escuela`, `ciudad`, `estado`, `inicio`, `termino`, `registro_id`, `updated_at`, `created_at`)
VALUES
	(1,'','escuela','ciudad','estado','inicio','termino',75,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'especialidad','escuela','ciudad','estado','inicio','termino',74,'2016-10-19 19:51:14','2016-10-19 19:51:14'),
	(3,'especialidad','escuela','ciudad','estado','inicio','termino',74,'2016-10-19 22:40:16','2016-10-19 22:40:16');

/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registro`;

CREATE TABLE `registro` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `membresia` varchar(20) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `token` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;

INSERT INTO `registro` (`id`, `user`, `password`, `membresia`, `updated_at`, `created_at`, `token`)
VALUES
	(1,'apiid2','fsdfsdf','','2016-10-18 01:43:47','0000-00-00 00:00:00',NULL),
	(4,'cambiosf34f33f13f3ff3fefef324f','4234234','','2016-10-17 23:09:04','0000-00-00 00:00:00',NULL),
	(5,'fr43','4234234','','2016-10-18 02:04:40','0000-00-00 00:00:00',NULL),
	(6,'fsdfrewf','ferfrefer','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(8,'zzzzzzzzzzzzzzzzz','mañana','','2016-10-18 02:11:30','0000-00-00 00:00:00',NULL),
	(13,'nuevo','nuevo','','2016-10-18 20:58:51','0000-00-00 00:00:00',NULL),
	(14,'macho','nuevo','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(15,'nuevo','1234','','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(16,'John','','','2016-10-13 19:54:49','2016-10-13 19:54:49',NULL),
	(17,'juan','John','','2016-10-18 20:39:21','2016-10-13 19:54:49',NULL),
	(18,'John','','','2016-10-13 19:55:16','2016-10-13 19:55:16',NULL),
	(19,'nuevo','John','','2016-10-18 20:41:35','2016-10-13 19:55:16',NULL),
	(20,'john@foo.com','','','2016-10-13 23:31:36','2016-10-13 19:55:55',NULL),
	(21,'nuevo','John','','2016-10-18 20:54:28','2016-10-13 19:55:55',NULL),
	(22,'John','','','2016-10-13 19:56:38','2016-10-13 19:56:38',NULL),
	(23,'nuevo','John','','2016-10-18 20:54:31','2016-10-13 19:56:38',NULL),
	(24,'John','','','2016-10-13 19:58:16','2016-10-13 19:58:16',NULL),
	(25,'nuevo','John','','2016-10-18 20:55:08','2016-10-13 19:58:16',NULL),
	(26,'John','','','2016-10-13 19:59:23','2016-10-13 19:59:23',NULL),
	(27,'nuevo','John','','2016-10-18 20:55:33','2016-10-13 19:59:23',NULL),
	(28,'John','','','2016-10-13 20:30:21','2016-10-13 20:30:21',NULL),
	(29,'nuevo','John','','2016-10-18 20:55:34','2016-10-13 20:30:21',NULL),
	(30,'John','John','','2016-10-13 20:31:29','2016-10-13 20:31:29',NULL),
	(31,'John','John','','2016-10-13 20:35:16','2016-10-13 20:35:16',NULL),
	(32,'John','John','','2016-10-13 20:36:55','2016-10-13 20:36:55',NULL),
	(33,'vientos de cambio','','','2016-10-13 23:36:23','2016-10-13 20:50:58',NULL),
	(34,'1','','','2016-10-13 20:53:55','2016-10-13 20:53:55',NULL),
	(35,'1','','','2016-10-13 20:54:07','2016-10-13 20:54:07',NULL),
	(36,'1','','','2016-10-13 20:55:13','2016-10-13 20:55:13',NULL),
	(37,'HTTP/1.1 200 OK\nContent-Type: ','','','2016-10-13 21:05:17','2016-10-13 21:05:17',NULL),
	(38,'HTTP/1.1 200 OK\nContent-Type: ','','','2016-10-13 21:06:55','2016-10-13 21:06:55',NULL),
	(39,'HTTP/1.1 200 OK\nContent-Type: ','','','2016-10-13 21:08:19','2016-10-13 21:08:19',NULL),
	(40,'$dataJson','','','2016-10-13 21:08:57','2016-10-13 21:08:57',NULL),
	(41,'$dataJson','','','2016-10-13 21:10:39','2016-10-13 21:10:39',NULL),
	(42,'$dataJson','','','2016-10-13 21:14:53','2016-10-13 21:14:53',NULL),
	(43,'fdlnbs','','','2016-10-13 21:32:22','2016-10-13 21:32:22',NULL),
	(44,'fdlnbs','','','2016-10-13 21:34:00','2016-10-13 21:34:00',NULL),
	(45,'fdlnbs','','','2016-10-13 21:34:59','2016-10-13 21:34:59',NULL),
	(46,'fdlnbs','','','2016-10-13 21:35:47','2016-10-13 21:35:47',NULL),
	(47,'fdlnbs','','','2016-10-13 21:37:40','2016-10-13 21:37:40',NULL),
	(48,'fdlnbs','','','2016-10-13 21:37:54','2016-10-13 21:37:54',NULL),
	(49,'fdlnbs','','','2016-10-13 21:40:23','2016-10-13 21:40:23',NULL),
	(50,'fdlnbs','','','2016-10-13 21:42:47','2016-10-13 21:42:47',NULL),
	(51,'fdlnbs','','','2016-10-13 21:44:02','2016-10-13 21:44:02',NULL),
	(52,'fdlnbs','','','2016-10-13 21:45:59','2016-10-13 21:45:59',NULL),
	(53,'fdlnbs','','','2016-10-13 21:46:43','2016-10-13 21:46:43',NULL),
	(54,'fdlnbs','','','2016-10-13 21:47:26','2016-10-13 21:47:26',NULL),
	(55,'fdlnbs','','','2016-10-13 21:48:35','2016-10-13 21:48:35',NULL),
	(56,'fdlnbs','','','2016-10-13 21:49:36','2016-10-13 21:49:36',NULL),
	(57,'fdlnbs','','','2016-10-13 21:50:06','2016-10-13 21:50:06',NULL),
	(58,'fdlnbs','','','2016-10-13 21:50:31','2016-10-13 21:50:31',NULL),
	(59,'fdlnbs','','','2016-10-13 21:50:49','2016-10-13 21:50:49',NULL),
	(60,'fewfewfewfe','','','2016-10-13 21:52:04','2016-10-13 21:52:04',NULL),
	(61,'fewfewfewfe','ferfwefewefr3333','','2016-10-13 21:53:00','2016-10-13 21:53:00',NULL),
	(62,'cambios','nombresifunciona','','2016-10-13 21:54:03','2016-10-13 21:54:03',NULL),
	(63,'vientos de cambio','funciona','','2016-10-13 23:51:07','2016-10-13 23:51:07',NULL),
	(64,'vientos de cambio','$2y$10$6evlDyIDSZ6D2OGBqZvcfuQ','','2016-10-13 23:58:21','2016-10-13 23:58:21',NULL),
	(65,'vientos de cambio','$2y$10$FLXDfNKQ9mOUGO/wQysh9uG','','2016-10-14 02:03:29','2016-10-14 02:03:29',NULL),
	(66,'vientos','$2y$10$hCs8HSwbLtVvaM2M0wTqvOQ','','2016-10-14 02:03:56','2016-10-14 02:03:56',NULL),
	(67,'juan','$2y$10$WgryaiGokH5BuDFgeiRv1Oe','','2016-10-15 00:09:36','2016-10-15 00:09:36',NULL),
	(69,'joseju','$2y$10$.gPv2rGODYarcv2X3FDPKeZHPlw/MC7FRiyp6VA4DwKX3JXhc/1bm','','2016-10-18 02:13:18','2016-10-15 00:22:10',NULL),
	(70,'apiid2','$2y$10$XPBCu4/XKd/m/yu0UnPMfu6LSeREFPwWPZpUOvNqVOpDZJvpLXnQ2','','2016-10-18 01:47:54','2016-10-17 18:28:56',NULL),
	(72,'juan','$2y$10$RIgrTWjDT3PLcEMuxl149OWagYqiZnh3hT0PDeo2ldRrRNDfJwniy','','2016-10-18 17:55:09','2016-10-18 17:55:09',NULL),
	(74,'chido','$2y$10$nkHWvqdnnvtd3W2QEFhjYOybmQ2os0alETy0IzrfZj7zSOKm9lvmC','','2016-10-19 19:48:12','2016-10-18 19:35:39','1fd974560f36f2f9993b4c586ce898b885fbb4c89472d9b3c871754239446562'),
	(75,'chido','$2y$10$bxmWvkBXKue6owx8y3VFRuUgDv0Oyqq4FtTcF0d.2qX5VwUFSYCGy','','2016-10-18 23:07:45','2016-10-18 23:00:23',NULL),
	(76,'admin2','$2y$10$IkFM5GMGc6Fx4/wN7eE0SOrtjhNiDIa8se4QWIeRXitI0uzECLWl2','premium','2016-10-19 20:57:30','2016-10-19 20:33:22','46432899639320981315b972fae8102354dfd9519ee34ce302180988a0054f3e'),
	(77,'admin','$2y$10$wTp0BenVZo1tllwccv6elOAsryyJg5KrzNyIf47y0ElMU/3lj2Hku','admin','2016-10-19 20:58:19','2016-10-19 20:37:26','ff391cd1b37505ef863af19860ce6fe8b1af7efe5c5f3c65129aa895055edb0b'),
	(78,'premium','$2y$10$6X3v2d3Ek8MGmijLol3Z9eAwWt6cXOXBnrvgS0D/Zl3D03H.q1GIO','premium','2016-10-19 20:37:49','2016-10-19 20:37:49',NULL),
	(79,'regular','$2y$10$IVbh0ZjzAisx6OCD/tZzReABhspRccVMgt/nbPLKmDaolDW2qJYq6','regular','2016-10-19 20:38:06','2016-10-19 20:38:06',NULL),
	(80,'regular2','$2y$10$wW1qSBndbP8hM9iZiFeY8eaZk4wrNcCLyuXYo/gWB3CksqWa94aeS','regular','2016-10-19 20:58:29','2016-10-19 20:58:29',NULL),
	(81,'regular2','$2y$10$4M8JzpYOUAH1N6OKIKJug.iaDHWrjYNXQSgygI6Uu.DJVN.C6ugti','recuperador','2016-10-19 21:28:01','2016-10-19 21:28:01',NULL);

/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
