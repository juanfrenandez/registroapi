<?php

namespace mvc\Lib;

class Constants {

    //Ordenados en orden alfabetico
    public $CODE_MEANING = 'Code_Meaning';
    public $IMAGE_TYPE = 'Image_Type';
    public $INSTANCE_CREATION_DATE = 'Instance_Creation_Date';
    public $INSTANCES_NUMBER = 'INSTANCES_NUMBER';
    public $INTERCEPT = 'Intercept';
    public $MANUFACTURER = 'Manufacturer';
    public $MANUFACTURER_MODEL_NAME = 'Manufacturer_Model_Name';
    public $MODALITY = 'Modality';
    public $PATIENT_BIRTH_DATE = 'Patient_Birth_Date';
    public $PATIENT_ID = 'Patient_Id';
    public $PATIENT_IDENTITY_REMOVED = 'Patient_Identity_Removed';
    public $PATIENT_NAME = 'Name_Patient';
    public $PIXEL_DATA = 'Pixel_Data';
    public $PIXEL_SPACING = 'Pixel_Spacing';
    PUBLIC $RESCALE_SLOPE = 'Rescale Slope';
    public $SERIES_DESCRIPTION = 'Series_Description';
    public $SERIES_UID = 'Series_UID';
    public $SOP_INSTANCE_UID = 'SOP_INSTANCE_UID';
    public $STUDY_DATE = 'Study_Date';
    public $STUDY_DESCRIPTION = 'Study_Description';
    public $STUDY_UID = 'Study_UID';
    public $SERIES_NUMBER = 'Series_Number';

    //Ordenados por grupos y elementos
    function getInfoFromTag($Tag){
        $tagToReturn = $elementsTag = array(
            $this->IMAGE_TYPE => [0x008, 0x0008],
            $this->INSTANCE_CREATION_DATE => [0x0008, 0x0012],
            $this->SOP_INSTANCE_UID => [0x0008, 0x0018],
            $this->STUDY_DATE => [0x0008, 0x0020],
            $this->MODALITY => [0x0008, 0x0060],
            $this->MANUFACTURER => [0x0008, 0x0070],
            $this->CODE_MEANING => [0x0008, 0x0104],
            $this->STUDY_DESCRIPTION => [0x0008, 0x1030],
            $this->SERIES_DESCRIPTION => [0x0008, 0x103E],
            $this->MANUFACTURER_MODEL_NAME => [0x0008, 0x1090], 
            $this->PATIENT_NAME => [0x0010, 0x0010],
            $this->PATIENT_ID => [0x0010, 0x0020],
            $this->PATIENT_BIRTH_DATE => [0x0010, 0x0030],
            $this->PATIENT_IDENTITY_REMOVED => [0x0012, 0x0062],
            $this->STUDY_UID => [0x0020, 0x000D],
            $this->SERIES_UID => [0x0020, 0x000E],
            $this->SERIES_NUMBER => [0x0020, 0x0011],
            $this->INSTANCES_NUMBER => [0x0020, 0x0013],
            $this->PIXEL_SPACING => [0x0028, 0x0030],
            $this->INTERCEPT => [0x0028, 0X1052],
            $this->RESCALE_SLOPE => [0x0028, 0X1053],
            $this->PIXEL_DATA => [0x7FE0, 0X0010]
        );

        return $tagToReturn[$Tag];
    }
}