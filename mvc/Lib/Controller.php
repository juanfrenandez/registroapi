<?php

namespace mvc\Lib;

use \mvc\Models\Usuario as Usuario;

use \Exception as Exception;

class Controller{

    protected $container = null;

    // function __construct($container)
    // {
    //     $this->container = $container;
    // }

    public function tr($data=null, $error=false, $msg=''){
        return array('error'=>$error, 'message'=>$msg, 'data'=> $data);
    }

    public function checkToken($request){

    	 $token = $request->getHeader('Authorization');

        if ($token == null) {
           /* $resultado = $this->tr(null, true, 'Authorization Header not found');*/

           /* return $resultado;*/
        	throw new Exception("Authorization Header not found", 1);
           
        }

        $userDB = Usuario::where('token', $token)->first();

        if ($userDB == null) {
            /*$resultado = $this->tr(null, true, 'token invalido');

            return $resultado;*/
            throw new Exception("Token invalido", 1);
        }

        if ($userDB->membresia == null) {
            /*$resultado = $this->tr(null, true, 'token invalido');

            return $resultado;*/
            throw new Exception("Error de usuario", 1);
        }

        return $userDB;
    }

    public function hiddenUser($users){

        $users->each(function($user)
        {

            if ($user->membresia != 'premium' && $user->membresia != 'regular') {
                $user->id = null;
                $user->user = null;
                $user->membresia = null;
                $user->created_at = null;
                $user->updated_at = null;
                $user->dato = null;
                $user->educacion = null;
                $user->direccion = null;
                $user->deleted_at = null;

                unset($user->id);
                unset($user->user);
                unset($user->membresia);
                unset($user->created_at);
                unset($user->updated_at);
                unset($user->dato);
                unset($user->educacion);
                unset($user->direccion);
                unset($user->deleted_at);
            }

        });
        
    }

    public function sort($users, $order='asc'){

        $users = Usuario::all();

        // $users->load(['dato','' => function($query)
        // {
        //     $query->orderBy('nombre', 'desc');

        // }])->get();

        $users = Usuario::with('dato.telefono','dato.mail','dato.operacion','educacion','direccion.horario')->orderBy($users, $order)->get();

        return $users;

    }

}