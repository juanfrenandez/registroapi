<?php

namespace mvc\Lib;

class ParserDicom extends \mvc\Lib\Constants {

    public function parserBin($dirDicomFile, $indexes){
        $file = fopen($dirDicomFile, "r");

        //stat[7] obtine el peso del archivo
        // $stat = fstat($file);

        $indexesLE16 = 8;
        $contDicom = fread($file, filesize($dirDicomFile));
        $indexElement = $indexes[0];
        $indexGroup = $indexes[1];

        $group = pack('v', $indexElement);
        $element = pack('v', $indexGroup);

        $indexTag = strpos($contDicom, $group . $element);
        $lengthTag = substr($contDicom, $indexTag, $indexesLE16);
        $binLength = unpack("C*", $lengthTag);
        $salidaTag = substr($contDicom, $indexTag + $indexesLE16, $binLength[7]);
        
        if(!is_string($salidaTag) || $salidaTag == ''){
            $salidaTag = 'Tag no encontrado';
        }
        
        return $salidaTag;
    }

    public function getPatientId($dirDicomFile){
        $element = new Constants();
        $indexes = $element->getInfoFromTag($element->PATIENT_ID);
        $tagPrint = $this->parserBin($dirDicomFile, $indexes);
        return $tagPrint;
    }

    public function getAnotherTag($dirDicomFile, $NameTag){
        $element = new Constants();
        $indexes = $element->getInfoFromTag($NameTag);
        $tagPrint = $this->parserBin($dirDicomFile, $indexes);
        return $tagPrint;
    }

    //Poder crear obtener los datos para la tabla de registro en el frontend
    public function getInfoPatientByRegistro($dirDicomFile){
        $elements = new Constants();
        $infoTags = array(
            $elements->PATIENT_ID,
            $elements->STUDY_DATE,
            $elements->PATIENT_NAME
        );

        $tagsForPatients = [];
        foreach ($infoTags as $tag) {
            $outputTag = $this->getAnotherTag($dirDicomFile, $tag);
            $tagsForPatients[$tag] = $outputTag;
        }

        return $tagsForPatients;
    }

    public function getMultipleTags($dirDicomFile, $tagsArray){
        $tagsForPatients = [];
        foreach ($tagsArray as $tag) {
            $outputTag = $this->getAnotherTag($dirDicomFile, $tag);
            $tagsForPatients[$tag] = $outputTag;
        }

        return $tagsForPatients;
    }

    public function setImageFromDCM($dirDicomFile, $name){
        $file = fopen($dirDicomFile, "r");

        //stat[7] obtine el peso del archivo
        // $stat = fstat($file);

        $indexesLE16 = 12;
        $contDicom = fread($file, filesize($dirDicomFile));
        $indexElement = 0x7FE0;
        $indexGroup = 0x0010;

        $group = pack('v', $indexElement);
        $element = pack('v', $indexGroup);

        $indexTag = strpos($contDicom, $group . $element);
        $lengthTag = substr($contDicom, $indexTag, $indexesLE16);
        $binLength = unpack("C*", $lengthTag);
        $salidaTag = substr($contDicom, $indexTag + $indexesLE16, 524288);
        $binSalida = unpack("C*", $salidaTag);

        $width = 512;
        $heigth = 512;

        $gd = @imagecreatetruecolor ($width, $heigth) 
            or die("Cannot Initialize new GD image stream");

        $i=1;
        for ($y=0; $y < $width; $y++) { 
            for ($x=0; $x < $heigth; $x++) {
                // $colorRand = rand(1, 50000);
                $gray = imagecolorallocate($gd, $binSalida[$i], $binSalida[$i], $binSalida[$i]); 
                imagesetpixel($gd, $x,$y, $gray);
                $i += 2;
            }
        }
        
        // imagesetpixel($gd, round($x),round($y), $rojo);
        $routeImage = "archivos/$name.png";
        header('Content-type: image/png');
        imagepng($gd, $routeImage);

        if (file_exists($routeImage)){
            return 1;
        }

        return 0;
    }

    public function parserBinPixel($dirDicomFile, $indexes){
        $file = fopen($dirDicomFile, "r");

        //stat[7] obtine el peso del archivo
        // $stat = fstat($file);

        $indexesLE16 = 12;
        $contDicom = fread($file, filesize($dirDicomFile));
        $indexElement = 0x7FE0;
        $indexGroup = 0x0010;

        $group = pack('v', $indexElement);
        $element = pack('v', $indexGroup);

        $indexTag = strpos($contDicom, $group . $element);
        $lengthTag = substr($contDicom, $indexTag, $indexesLE16);
        $binLength = unpack("C*", $lengthTag);
        $salidaTag = substr($contDicom, $indexTag + $indexesLE16, 524288);
        $binSalida = unpack("C*", $salidaTag);

        $width = 512;
        $heigth = 512;

        $gd = @imagecreatetruecolor ($width, $heigth) 
            or die("Cannot Initialize new GD image stream");

        $i=1;
        for ($y=0; $y < $width; $y++) { 
            for ($x=0; $x < $heigth; $x++) {
                // $colorRand = rand(1, 50000);
                $gray = imagecolorallocate($gd, $binSalida[$i], $binSalida[$i], $binSalida[$i]); 
                imagesetpixel($gd, $x,$y, $gray);
                $i += 2;
            }
        }
        
        // imagesetpixel($gd, round($x),round($y), $rojo);
        header('Content-type: image/png');
        imagepng($gd, 'archivos/reciente.png');

        return $gd;
    }

    public function getImages($dirDicomFile, $NameTag){
        $element = new Constants();
        $indexes = $element->getInfoFromTag($NameTag);
        $tagPrint = $this->parserBinPixel($dirDicomFile, $indexes);
        return $tagPrint;
    }

    public function getImage($dirDicomFile, $tagsArray){
        $tagsForPatients = [];
        foreach ($tagsArray as $tag) {
            $outputTag = $this->getImages($dirDicomFile, $tag);
            $tagsForPatients[$tag] = $outputTag;
        }

        return $tagsForPatients;
    }

    public function setImageInServer($dirDicomFile, $name){
        $file = fopen($dirDicomFile, "r");

        //stat[7] obtine el peso del archivo
        // $stat = fstat($file);

        $indexesLE16 = 12;
        $contDicom = fread($file, filesize($dirDicomFile));
        $indexElement = 0x7FE0;
        $indexGroup = 0x0010;

        $group = pack('v', $indexElement);
        $element = pack('v', $indexGroup);

        $indexTag = strpos($contDicom, $group . $element);
        $lengthTag = substr($contDicom, $indexTag, $indexesLE16);
        $binLength = unpack("C*", $lengthTag);
        $salidaTag = substr($contDicom, $indexTag + $indexesLE16, 524288);
        $binSalida = unpack("C*", $salidaTag);

        $width = 512;
        $heigth = 512;

        $gd = @imagecreatetruecolor ($width, $heigth) 
            or die("Cannot Initialize new GD image stream");

        $i=1;
        for ($y=0; $y < $width; $y++) { 
            for ($x=0; $x < $heigth; $x++) {
                // $colorRand = rand(1, 50000);
                $gray = imagecolorallocate($gd, $binSalida[$i], $binSalida[$i], $binSalida[$i]); 
                imagesetpixel($gd, $x,$y, $gray);
                $i += 2;
            }
        }
        
        // imagesetpixel($gd, round($x),round($y), $rojo);
        header('Content-type: image/png');

        $route = "archivos/$name.png";

        imagepng($gd, $route);

        return $route;
    }

}