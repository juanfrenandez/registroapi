<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Educacion extends Model {

    protected $table = 'educacion';

    protected $fillable = [
        'carrera',
        'escuela',
        'ciudad',
        'estado',
        'inicio',
        'termino',
        'registro_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function usuario()
    {
        return $this->belongsTo('Usuario', 'fk_educacion_registro1', 'registro_id');
    }

}