<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Dato extends Model {

    protected $table = 'dato';

    protected $fillable = [
        'nombre',
        'apellido',
        'descripcion',
        'titulo',
        'cedula',
        'natal',
        'natal_estado',
        'fb',
        'web',
        'image',
        'email',
        'registro_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function usuario()
    {
        return $this->belongsTo('Usuario', 'fk_direccion_registro', 'registro_id');
    }

    public function telefono()
    {
        return $this->hasMany('mvc\Models\Telefono', 'dato_id');
    }

    public function mail()
    {
        return $this->hasMany('mvc\Models\Mail', 'dato_id');
    }

    public function operacion()
    {
        return $this->hasMany('mvc\Models\Operacion', 'dato_id');
    }

}