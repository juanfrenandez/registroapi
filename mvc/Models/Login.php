<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Login extends Model {

    protected $table = 'registro';

    protected $fillable = [
        'user',
        'password'
    ];

}