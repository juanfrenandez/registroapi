<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Operacion extends Model {

    protected $table = 'operaciones';

    protected $fillable = [
        'tag',
        'empresa',
        'inicio',
        'termino',
        'dato_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function dato()
    {
        return $this->belongsTo('Dato', 'fk_operacion_dato', 'dato_id');
    }

    // public function horario()
    // {
    //     return $this->hasMany('mvc\Models\Horario', 'direccion_id');
    // }

}