<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Telefono extends Model {

    protected $table = 'telefonos';

    protected $fillable = [
        'info',
        'numero',
        'dato_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function dato()
    {
        return $this->belongsTo('Dato');
    }

    // public function horario()
    // {
    //     return $this->hasMany('mvc\Models\Horario', 'direccion_id');
    // }

}