<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Study extends Model {

    protected $table = 'studies';

    protected $fillable = [
        'study_instance_uid',
        'study_description',
        'registro_id'
    ];

}