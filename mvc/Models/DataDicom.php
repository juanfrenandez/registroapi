<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Datadicom extends Model {

    protected $table = 'dicom_files';

    protected $fillable = [
        'link',
        'registro_id'
    ];

}