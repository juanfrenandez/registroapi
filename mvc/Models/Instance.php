<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Instance extends Model {

    protected $table = 'instances';

    protected $fillable = [
        'instance_uid',
        'file_size',
        'index_series',
        'instance_number',
        'link_dicom',
        'study_id',
        'series_id',
        'registro_id'
    ];

}