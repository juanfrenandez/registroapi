<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\SoftDeletes as SoftDeletes;

use Illuminate\Database\Eloquent\Model as Model;

class Usuario extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $hidden = ['password','token'];

    protected $table = 'registro';

    protected $fillable = [
        'user',
        'password',
        'membresia',
        'email',
        'token',
        'register',
        'active',
        'patient_id',
        'typo_user',
        'license_id',
        'expire_license'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function getPdw()
    {
    	return $this->password;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function direccion()
    {
        return $this->hasMany('mvc\Models\Direccion', 'registro_id');
    }

    public function educacion()
    {
        return $this->hasMany('mvc\Models\Educacion', 'registro_id');
    }

    public function dato()
    {
        return $this->hasOne('mvc\Models\Dato', 'registro_id');
    }
}