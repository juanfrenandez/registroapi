<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Mail extends Model {

    protected $table = 'mails';

    protected $fillable = [
        'mail',
        'dato_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function dato()
    {
        return $this->belongsTo('Dato', 'fk_mail_dato', 'dato_id');
    }

    // public function horario()
    // {
    //     return $this->hasMany('mvc\Models\Horario', 'direccion_id');
    // }

}