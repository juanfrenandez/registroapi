<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Series extends Model {

    protected $table = 'series';

    protected $fillable = [
        'serie_uid',
        'series_description',
        'study_id',
        'registro_id'
    ];

}