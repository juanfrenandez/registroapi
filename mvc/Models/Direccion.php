<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Direccion extends Model {

    protected $table = 'direccion';

    protected $fillable = [
        'calle',
        'numero',
        'maps',
        'colonia',
        'ciudad',
        'estado',
        'registro_id'


    ];

    public function getName()
    {
    	return $this->user;
    }

    public function usuario()
    {
        return $this->belongsTo('Usuario', 'fk_direccion_registro', 'registro_id');
    }

    public function horario()
    {
        return $this->hasMany('mvc\Models\Horario', 'direccion_id');
    }

}