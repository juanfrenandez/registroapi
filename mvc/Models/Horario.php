<?php
namespace mvc\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Horario extends Model {

    protected $table = 'horarios';

    protected $fillable = [
        'dia', 
        'inicio',
        'dia_final',
        'cierre',
        'direccion_id'
    ];

    public function getName()
    {
    	return $this->user;
    }

    public function usuario()
    {
        return $this->belongsTo('Direccion');
    }

}