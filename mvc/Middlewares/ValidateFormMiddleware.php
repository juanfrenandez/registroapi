<?php

namespace mvc\Middlewares;

use mvc\Lib\Controller as Controller;

/**
* 
*/
class ValidateFormMiddleware extends Controller
{

	public function __invoke($request,$response, $next)
	{

		$check = $request->getParsedBody();
		$validate = $check['user'];
		foreach ($check as $input => $value) {
			if(!preg_match('/^[0-9a-z\!@\$\&\'",\.\?\/\s\-]*$/i', $value))
			{

				$resultado = $this->tr(null, true, 'solo puedes agregar letras y números');

				return $response->withJson($resultado, 404);

			}
		}

		$response = $next($request, $response);

		return $response;

	}
}