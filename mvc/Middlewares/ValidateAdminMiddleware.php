<?php  

namespace mvc\Middlewares;

use mvc\Lib\Controller as Controller;

use mvc\Middlewares\ValidateTokenMiddleware as ValidateTokenMiddleware;

/**
* 
*/
class ValidateAdminMiddleware extends ValidateTokenMiddleware
{

    public function __invoke($request, $response, $next){


    	$userDB = $this->ValidateToken($request,$response);

        if ($userDB->membresia == null || $userDB->membresia != 'admin') {
            $resultado = Controller::tr(null, true, 'token invalido');

            return $response->withJson($resultado);
        }

        $response->$userDB;
        $response = $next($request, $response);
        // $response->write('AFTER2');

        return $response;
    }
}


