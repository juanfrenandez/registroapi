<?php 

namespace mvc\Middlewares;

use mvc\Models\Usuario as Usuario;

/**
* 
*/
class ValidateTokenMiddleware
{
	
	protected function ValidateToken($request,$response)
	{
		$token = $request->getHeader('Authorization');

		if ($token == null) {

			return false;
			// return $response->withJson($resultado);
			// throw new Exception("Authorization Header not found", 1);

		}

		$userDB = Usuario::where('token', $token)->first();

		if ($userDB == null) {

			return false;

			// return $response->withJson($resultado);
			// throw new Exception("Token invalido", 1);
		}

		return $userDB;
	}
}