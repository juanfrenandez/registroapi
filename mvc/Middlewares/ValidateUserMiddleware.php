<?php  

namespace mvc\Middlewares;

use mvc\Lib\Controller as Controller;

use mvc\Middlewares\ValidateTokenMiddleware as ValidateTokenMiddleware;

/**
* 
*/
class ValidateUserMiddleware extends ValidateTokenMiddleware
{

    public function __invoke($request, $response, $next){

    	$validate = $this->ValidateToken($request,$response);

        if ($validate == false) {
            $resultado = Controller::tr(null, true, 'Token invalido');

            return $response->withJson($resultado);
        }

        $response = $next($request, $response);
        // $response->withJson('');

        return $response;

    }
}