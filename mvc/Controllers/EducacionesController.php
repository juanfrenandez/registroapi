<?php
namespace mvc\Controllers;

use \mvc\Models\Educacion as Educacion;

use \mvc\Models\Usuario as Usuario;

class EducacionesController extends \mvc\Lib\Controller
{
    // public function getPacientes ( $request,  $response, $args) {
    
    //     $data = array('name' => 'no name');

    //     return  $response->withJson($data);
    // }

    public function setEducacion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userForm = $request->getParsedBody();

        if ($userForm['carrera'] == null || $userForm['escuela'] == null || $userForm['ciudad'] == null || $userForm['estado'] == null || $userForm['inicio'] == null || $userForm['termino'] == null) {

            $resultado = $this->tr(null, true, 'Ingresa todos los campos');

            return  $response->withJson($resultado, 201);

        }

        // $userRegistro = Usuario::where('user', $userDB->user)->first();

        $userRegDireccion = Educacion::create([
            'carrera' => $userForm['carrera'], 
            'escuela' => $userForm['escuela'], 
            'ciudad' => $userForm['ciudad'], 
            'estado' => $userForm['estado'], 
            'inicio' => $userForm['inicio'], 
            'termino' => $userForm['termino'], 
            'registro_id' => $userDB->id
        ]);

        $resultado = $this->tr(null, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);

    }

    public function setEducacionAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $userForm = $request->getParsedBody();

        if ($userForm['carrera'] == null || $userForm['escuela'] == null || $userForm['ciudad'] == null || $userForm['estado'] == null || $userForm['inicio'] == null || $userForm['termino'] == null) {

            $resultado = $this->tr(null, true, 'Ingresa todos los campos');

            return  $response->withJson($resultado, 404);

        }

        // $userRegistro = Usuario::where('user', $userDB->user)->first();

        $userRegDireccion = Educacion::create([
            'carrera' => $userForm['carrera'], 
            'escuela' => $userForm['escuela'], 
            'ciudad' => $userForm['ciudad'], 
            'estado' => $userForm['estado'], 
            'inicio' => $userForm['inicio'], 
            'termino' => $userForm['termino'], 
            'registro_id' => $args['id']
        ]);

        $resultado = $this->tr(null, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);

    }

    public function getEducacion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado);
        }

       $roles = Usuario::all();

       $resultado = $this->tr($roles, false, '');

        return  $response->withJson($resultado);
    }

    public function updateEducacion ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado);
        }

        $userForm = $request->getParsedBody();

        $educacionDB = Educacion::where('id', $userForm['id'])->first();

        if ($userForm['carrera'] == null || $userForm['escuela'] == null || $userForm['ciudad'] == null || $userForm['estado'] == null || $userForm['inicio'] == null || $userForm['termino'] == null) {

            $resultado = $this->tr(null, true, 'Rellena todos los datos');

            return  $response->withJson($resultado, 201);

        }

        if ($userDB->membresia != 'admin') {
                if ($userDB->id != $educacionDB->registro_id) {
                $resultado = $this->tr(null, true, 'No se puede cambiar esta Educacion');

                return $response->withJson($resultado, 404);
            }
        }

        $userForm = $request->getParsedBody();

        $educacionDB->carrera = $userForm['carrera'];
        $educacionDB->escuela = $userForm['escuela'];
        $educacionDB->ciudad = $userForm['ciudad'];
        $educacionDB->estado = $userForm['estado'];
        $educacionDB->inicio = $userForm['inicio'];
        $educacionDB->termino = $userForm['termino'];

        $educacionDB->save();

        $resultado = $this->tr($educacionDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function deleteEducacion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado);
        }

        $deleteForm = $request->getParsedBody();

        $deleteDB = Educacion::find($deleteForm['id']);

        if ($deleteDB == null) {
            $resultado = $this->tr(null, true, 'Error al borrar Educacion');

            return $response->withJson($resultado,404);
        }

        if ($deleteDB->registro_id != $userDB->id) {
            $resultado = $this->tr(null, true, 'No se ha borrado esta Educacion');

            return $response->withJson($resultado,404);
        }

       $ifDestroy = Educacion::destroy($deleteDB->id);

       $resultado = $this->tr(null, false, 'El registro Educacion ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

}
