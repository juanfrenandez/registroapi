<?php
namespace mvc\Controllers;
use \Exception as Exception;
use \mvc\Lib\ParserDicom as ParserDicom;
use \mvc\Models\Usuario as Usuario;
use \mvc\Lib\Constants as Constants;
use \mvc\Models\Study as Study;
use \mvc\Models\Series as Series;
use \mvc\Models\Instance as Instance;

class PruebaDicomController extends \mvc\Lib\Controller
{

    public function savePatient ($infoPatient) {
        $ct = new Constants;
        $userName = $infoPatient[$ct->PATIENT_NAME];
        $patientId = $infoPatient[$ct->PATIENT_ID];
        $validateUserDup = Usuario::where([
            'user' => $userName,
            'patient_id' => $patientId
        ])->first();

        if ($validateUserDup != null) {
            return $validateUserDup;
        }

        $passEncripted = password_hash($infoPatient[$ct->STUDY_DATE], PASSWORD_DEFAULT);
        $createUser = array(
            'user' => $userName, 
            'password' => $passEncripted, 
            'membresia' => 'regular',
            'active' => 1,
            'patient_id' => $patientId,
            'typo_user' => 'Patient',
            'license_id' => 'dnekdneddked'
        );

        $user = Usuario::create($createUser);
        return  $user;
    }

    public function saveStudy($infoStudy, $registroId) {
        $ct = new Constants;
        $studyUID = $infoStudy[$ct->STUDY_UID];
        $studyDescription = $infoStudy[$ct->STUDY_DESCRIPTION];
        $studyDB = array(
            'study_instance_uid' => $studyUID,
            'registro_id' => $registroId
        );
        $validateStudyDup = Study::where($studyDB)->first();

        if ($validateStudyDup != null) {
            return $validateStudyDup;
        }
        $studyDB['study_description'] = $studyDescription;
        $study = Study::create($studyDB);
        
        return  201;
    }

    public function saveSerie($infoSerie, $registroId, $studyId) {
        
        $ct = new Constants;
        $serieDB = array(
            'serie_uid' => $infoSerie[$ct->SERIES_UID],
            'registro_id' => $registroId
        );

        $validateSerieDup = Series::where($serieDB)->first();

        if ($validateSerieDup != null) {
            return $validateSerieDup;
        }
        
        $serieDB['study_id'] = $studyId;
        $serieDB['series_description'] = $infoSerie[$ct->SERIES_DESCRIPTION];
        $serie = Series::create($serieDB);
        
        return  $serie;
    }

    public function saveInstancesAndFile($dirDicomFile, $infoInstance, $registroId, $studyId, $serieId) {

        try {
            
            $ct = new Constants;
            $parserDicom = new ParserDicom();

            $instanceID = $infoInstance[$ct->SOP_INSTANCE_UID];

            $routeFile = "archivos/$instanceID.dcm";
            // $dirDicomFile->moveTo($routeFile);
            move_uploaded_file($dirDicomFile, $routeFile);

            $resImage = $parserDicom->setImageFromDCM($routeFile, $instanceID);

            $instanceDB = array(
                'instance_uid' => $instanceID,
                'registro_id' => $registroId
            );

            $validateInstanceDup = Instance::where($instanceDB)->first();

            if ($validateInstanceDup != null) {
                return $validateInstanceDup;
            }

            $instanceNumber = intval($infoInstance[$ct->INSTANCES_NUMBER]);
            $instanceDB['file_size'] = $infoInstance['file_size'];
            $instanceDB['index_series'] = $infoInstance[$ct->SERIES_NUMBER];
            $instanceDB['instance_number'] = $instanceNumber;
            $instanceDB['link_dicom'] = $routeFile;
            $instanceDB['study_id'] = $studyId;
            $instanceDB['series_id'] = $serieId;
            $serie = Instance::create($instanceDB);
            
            return  $serie;

        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function setPDF ( $request,  $response, $args) {
    	try {
            
            $getContent = $request->getParsedBody();

            // $pdf_base64 = "archivos/juan.txt";
            // //Get File content from txt file
            // $pdf_base64_handler = fopen($pdf_base64,'r');
            // $pdf_content = fread ($pdf_base64_handler,filesize($pdf_base64));
            // fclose ($pdf_base64_handler);
            $studiyId = $getContent['id'];
            $dataStudy = Study::find($studiyId);
            $dataUid = $dataStudy->study_instance_uid;
            $explode = explode(",", $getContent['pdf']);
            //Decode pdf content
            $pdf_decoded = base64_decode ($explode[1]);
            //Write data back to pdf file
            $pdf = fopen ("archivos/$dataUid.pdf",'w');
            $rest = fwrite ($pdf,$pdf_decoded);
            //close output file
            fclose ($pdf);
            return $response->withJson(['message'=> $dataStudy]);

        }catch(Exception $e){
            return $response->withJson($e->getMessage(), 500);
        }
    }

    public function getData ( $request,  $response, $args) {
    	try {
            $files = $request->getUploadedFiles();
            $dicomFile = $files['value'];
            if (empty($files['value'])) {
                throw new Exception('Expected a value');
            }

            $dirDicomFile = $files['value']->file;
            $directoryTemp = $tmp_name = $_FILES["value"]["tmp_name"];
            $dicomFileSize = filesize($dirDicomFile);
            $parserDicom = new ParserDicom();

            // $routeFile = "archivos/788999s.dcm";
            // $check = move_uploaded_file($ner, $routeFile);

            $ct = new Constants;
            $tagArray = array(
                $ct->PATIENT_ID,
                $ct->STUDY_DATE,
                $ct->PATIENT_NAME,
                $ct->STUDY_UID,
                $ct->STUDY_DESCRIPTION,
                $ct->SERIES_UID,
                $ct->SERIES_DESCRIPTION,
                $ct->SOP_INSTANCE_UID,
                $ct->SERIES_NUMBER,
                $ct->INSTANCES_NUMBER
            );
            $infoPatient = $parserDicom->getMultipleTags($dirDicomFile, $tagArray);
            $infoPatient['file_size'] = $dicomFileSize;
            $registerResponse = $this->savePatient($infoPatient);
            $registroId = $registerResponse->id;
            
            if($registroId > 0){

                $studyResponse = $this->saveStudy($infoPatient, $registroId);
                $studyId = $studyResponse->id;
                if($studyId > 0){

                    $serieResponse = $this->saveSerie($infoPatient, $registroId, $studyId);
                    $serieId = $serieResponse->id;

                    if($serieId > 0){
                        $instancesResponse = $this->saveInstancesAndFile($directoryTemp, $infoPatient, $registroId, $studyId, $serieId);
                    }
                }
            }
            return $response->withJson(['message'=> $infoPatient]);

        }catch(Exception $e){
            return $response->withJson($e->getMessage(), 500);
        }
    }

    public function getPixelData ( $request,  $response, $args) {
    	try {
            $files = $request->getUploadedFiles();
            $dicomFile = $files['value'];
            if (empty($files['value'])) {
                throw new Exception('Expected a value');
            }

            $dirDicomFile = $files['value']->file;
            $dicomFileSize = filesize($dirDicomFile);
            $ct = new Constants;
            $parserDicom = new ParserDicom();

            $tagArray = array(
                $ct->SOP_INSTANCE_UID
            );

            $uuidInstance = $parserDicom->getMultipleTags($dirDicomFile, $tagArray);

            $imagesRoute = $parserDicom->setImageInServer($dirDicomFile, $uuidInstance['SOP_INSTANCE_UID']);

            if(file_exists($imagesRoute)){
                $imageRes = new \GuzzleHttp\Psr7\LazyOpenStream($imagesRoute, 'r');
                return $response->withHeader('Content-type', 'image/png')->withBody($imageRes);
                // return $response->withJson($uuidInstance['SOP_INSTANCE_UID']);
            }else{
                return $response->withJson('no jaló este pedo', 500);
            }

        }catch(Exception $e){
            // return $response->withHeader('Content-type', 'image/png');
            return $response->withJson($e->getMessage(), 500);
        }
    }

    public function saveDCM ( $request,  $response, $args) {
    	try {
            $files = $request->getUploadedFiles();
            $dicomFile = $files['value'];
            if (empty($files['value'])) {
                throw new Exception('Expected a value');
            }

            $dirDicomFile = $files['value']->file;
            $dicomFileSize = filesize($dirDicomFile);
            move_uploaded_file($dirDicomFile, 'archivos/67.dcm');
            // $ct = new Constants;
            // $parserDicom = new ParserDicom();

            // $tagArray = array(
            //     $ct->SOP_INSTANCE_UID
            // );

            // if(file_exists($imagesRoute)){
            //     $imageRes = new \GuzzleHttp\Psr7\LazyOpenStream($imagesRoute, 'r');
            //     return $response->withHeader('Content-type', 'image/png')->withBody($imageRes);
            //     // return $response->withJson($uuidInstance['SOP_INSTANCE_UID']);
            // }else{
            //     return $response->withJson('no jaló este pedo', 500);
            // }
            
            return $response->withJson('no jaló este pedo');
            // return $response->withHeader('Content-type', 'image/png')->withBody($imageRes);

        }catch(Exception $e){
            // return $response->withHeader('Content-type', 'image/png');
            return $response->withJson($e->getMessage(), 500);
        }
    }

}