<?php
namespace mvc\Controllers;

use \mvc\Models\Dato as Dato;

use \mvc\Models\Usuario as Usuario;

class DatosController extends \mvc\Lib\Controller
{

    public function setDatos ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userData = $request->getParsedBody();

        $maxData = Dato::where('registro_id', $userDB->id)->count();

        if ($maxData >= 1) {
            $resultado = $this->tr(null, true, 'Solo puedes crear un registro dato');

            return $response->withJson($resultado, 404);
        }

        if ($userData == null || $userData['nombre'] == null || $userData['apellido'] == null || $userData['descripcion'] == null || $userData['cedula']  == null || $userData['natal']  == null || $userData['titulo']  == null || $userData['email']  == null) {

            $resultado = $this->tr(null, true, 'No se han agregado todos los registros mensho');

            return $response->withJson($resultado, 404);
        }

        if ($userData == null) {
            $resultado = $this->tr(null, true, 'Datos not found');

            return $response->withJson($resultado, 404); 
        }

        $userRegData = Dato::create([
            'nombre' => $userData['nombre'],  
            'apellido' => $userData['apellido'],
            'email' => $userData['email'],
            'descripcion' => $userData['descripcion'], 
            'titulo' => $userData['titulo'],
            'cedula' => $userData['cedula'],
            'natal' => $userData['natal'],
            'natal_estado' => $userData['natal_estado'],
            'fb' => $userData['fb'],
            'web' => $userData['web'],
            'registro_id' => $userDB->id
        ]);

        $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function setDatosAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $userData = $request->getParsedBody();

        $maxData = Dato::where('registro_id', $args['id'])->count();

        if ($maxData >= 1) {
            $resultado = $this->tr(null, true, 'Solo puedes crear un registro dato');

            return $response->withJson($resultado, 404);

        }

        if ($userData == null || $userData['nombre'] == null || $userData['apellido'] == null || $userData['descripcion'] == null || $userData['cedula']  == null || $userData['natal']  == null || $userData['titulo']  == null || $userData['email']  == null) {

            $resultado = $this->tr(null, true, 'No se han agregado todos los registros mensho');

            return $response->withJson($resultado, 404);
        }

        if ($userData == null) {
            $resultado = $this->tr(null, true, 'Datos not found');

            return $response->withJson($resultado, 404); 
        }

        $userRegData = Dato::create([
            'nombre' => $userData['nombre'],  
            'apellido' => $userData['apellido'],
            'email' => $userData['email'],
            'descripcion' => $userData['descripcion'], 
            'titulo' => $userData['titulo'],
            'cedula' => $userData['cedula'],
            'natal' => $userData['natal'],
            'natal_estado' => $userData['natal_estado'],
            'fb' => $userData['fb'],
            'web' => $userData['web'],
            'registro_id' => $args['id']
        ]);

        $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function getDatos ( $request, $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado, 404);
        }

       $horario = Dato::where('registro_id', $userDB->id)->first();

       $resultado = $this->tr($horario->id, false, '');

        return  $response->withJson($resultado);
    }

    public function putDatos ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $updateDB->nombre = $updateForm['nombre'];
        $updateDB->apellido = $updateForm['apellido'];
        $updateDB->mail = $updateForm['mail'];
        $updateDB->descripcion = $updateForm['descripcion'];
        $updateDB->titulo = $updateForm['titulo'];
        $updateDB->cedula = $updateForm['cedula'];
        $updateDB->natal = $updateForm['natal'];

        $updateDB->save();

        $resultado = $this->tr($updateDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);
    }

    public function putDatosAdmin ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {

            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Dato::where('registro_id', $args['id'])->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $updateDB->nombre = $updateForm['nombre'];
        $updateDB->apellido = $updateForm['apellido'];
        $updateDB->email = $updateForm['email'];
        $updateDB->descripcion = $updateForm['descripcion'];
        $updateDB->titulo = $updateForm['titulo'];
        $updateDB->cedula = $updateForm['cedula'];
        $updateDB->natal = $updateForm['natal'];
        $updateDB->natal_estado = $updateForm['natal_estado'];
        $updateDB->fb = $updateForm['fb'];
        $updateDB->web = $updateForm['web'];

        $updateDB->save();

        $resultado = $this->tr($updateDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);
    }

    public function deleteDatos ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado);
        }

        $deleteDB = Dato::where('registro_id', $userDB->id)->first();

        if ($deleteDB == null) {
            $resultado = $this->tr(null, true, 'Error al eliminar datos');

            return $response->withJson($resultado, 404);
        }

        $ifDestroy = Dato::destroy($deleteDB->id);

        $resultado = $this->tr(null, false, 'El registro datos ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

    public function uploadImage ($request,$response,$args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $baseServer = 'http://64.64.6.57/~amcperapi/';

        $data = $_FILES['archivo']['name'];

        $ruta = '/~amcperapi/archivos/';

        $fichero_subido = $ruta . basename($data);
        // if (is_dir($ruta) && is_writable($ruta)) {
        //     $resultado = $this->tr(null, false, 'si tienes permisos');

        //     return $response->withJson($resultado);
        // } else {
        //     $resultado = $this->tr(null, true, '');

        //     return $response->withJson($resultado);
        // }

        if ($data == null) {

            $resultado = $this->tr(null, true, 'Inserta alguna imagen');

            return $response->withJson($resultado, 404);

        }

        $formato =  array('gif','png' ,'jpg');
        $ext = pathinfo($data, PATHINFO_EXTENSION);
        if(!in_array($ext,$formato) ) {
            $resultado = $this->tr(null, true, 'El archivo no es una imagen');

            return $response->withJson($resultado, 404);
        }

        if ($_FILES["archivo"]["size"] > 500000) {
            $resultado = $this->tr(null, true, 'La imagen es demasiado grande');

            return $response->withJson($resultado, 404);
        }

        if (file_exists($target_file)) {
            $resultado = $this->tr(null, true, 'El archivo ya existe');

            return $response->withJson($resultado, 404);
        }

        if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {

            $putImage = Dato::where('registro_id', $userDB->id)->first();

            $putImage->image = $baseServer . $ruta . $data;

            $putImage->save();

            $resultado = $this->tr(null, false, 'La imagen ha sido enviada');

            return $response->withJson($resultado);

        } else {

            $comporbacion = move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);

            $resultado = $this->tr(null, error, $fichero_subido);

            return $response->withJson($resultado);

        }

    }

}
