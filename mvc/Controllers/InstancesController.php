<?php
namespace mvc\Controllers;
use \mvc\Models\Instance as Instance;
use \mvc\Models\Usuario as Usuario;
use \Exception as Exception;
use \mvc\Lib\ParserDicom as ParserDicom;
use \mvc\Lib\Constants as Constants;

class InstancesController extends \mvc\Lib\Controller
{
    public function setData ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $createInstance = array(
            'instance_id' => $getContent['instance_id'],
            'file_size' => $getContent['file_size'],  
            'file_uuid' => $getContent['file_uuid'],
            'index_series' => $getContent['index_series'],
            'link_dicom' => $getContent['link_dicom'],
            'patient_id' => $getContent['patient_id']
        );

        try {
            $user = Instance::create($createInstance);

            $data = $this->tr($user);
            return  $response->withJson($data, 201);
        }catch(Exception $e){
            return  $response->withJson($e->getMessage(), 500);
        }

    }

    public function getData ( $request, $response, $args) {

        // try {
        //     $userDB = $this->checkToken($request);
        // } catch (\Exception $e) {
        //     $resultado = $this->tr(null, true, $e->getMessage());


        //     return $response->withJson($resultado, 404);
        // }

        $user = $args['user'];

        $userResponse = Usuario::where('user', $user)->first();

        $instances = Instance::where('registro_id', $userResponse->id)->get();

        $resultado = $this->tr($instances, false, '');

        return  $response->withJson($resultado);
    }

    public function getInstancesBySerie ( $request, $response, $args) {

        $instances = $args['id'];

        $instancesResponse = Instance::where('series_id', $instances)->orderBy('instance_number', 'asc')->get();

        $resultado = $this->tr($instancesResponse, false, '');

        return  $response->withJson($resultado);
    }

    public function getImage ( $request, $response, $args) {

        $instancesResponse = Instance::where('id', $args['id'])->first();

        $file = fopen($instancesResponse->link_dicom, "r");

        //stat[7] obtine el peso del archivo
        // $stat = fstat($file);
        $contDicom = fread($file, filesize($instancesResponse->link_dicom));

        // return  $response->withJson($contDicom);
        return $response->withHeader('Content-type', 'application/dicom')->write($contDicom)->withStatus(200);
    }

    public function getTags ( $request, $response, $args) {

        $instancesResponse = Instance::where('id', $args['id'])->first();

        $parserDicom = new ParserDicom();

        $ct = new Constants;
        $tagArray = array(
            $ct->PATIENT_ID,
            $ct->STUDY_DATE,
            $ct->PATIENT_NAME,
            $ct->STUDY_UID,
            $ct->STUDY_DESCRIPTION,
            $ct->SERIES_UID,
            $ct->SERIES_DESCRIPTION,
            $ct->SOP_INSTANCE_UID,
            $ct->SERIES_NUMBER,
            $ct->MODALITY,
            $ct->MANUFACTURER,
            $ct->IMAGE_TYPE,
            $ct->INSTANCE_CREATION_DATE,
            $ct->MANUFACTURER_MODEL_NAME,
            $ct->CODE_MEANING,
            $ct->PATIENT_IDENTITY_REMOVED,
            $ct->PIXEL_SPACING,
            $ct->RESCALE_SLOPE,
            $ct->INTERCEPT
        );

        $infoPatient = $parserDicom->getMultipleTags($instancesResponse->link_dicom, $tagArray);

        return $response->withJson($infoPatient);
    }

    public function setImage ($request, $response, $args) {
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['value'];
        $name = $args['name'];
        $routeFile = "archivos/$name.jpg";
        try {
            $uploadedFile->moveTo($routeFile);
            $response->withJson('si');
        }catch(Exception $e) {
            return  $response->withJson('no', 500);
        }
    }
}