<?php
namespace mvc\Controllers;

use \mvc\Models\Datadicom as Datadicom;
use \Firebase\JWT\JWT;

use \Exception as Exception;

class DatadicomsController extends \mvc\Lib\Controller
{
    public function setData ( $request,  $response, $args) {

    	$files = $request->getUploadedFiles();
        if (empty($files['value'])) {
            throw new Exception('Expected a value');
        }

        $newfile = $files['value'];
        // do something with $newfile
        $uploadFileName = $newfile->getClientFilename();
        try {
            $newfile->moveTo("archivos/$uploadFileName");

            return $response->withJson(['message'=> 'Archivo subido correctamente']);

        }catch(Exception $e){

            return $response->withJson($e->getMessage(), 500);

        }
    }
}