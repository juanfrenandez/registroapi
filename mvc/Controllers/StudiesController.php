<?php
namespace mvc\Controllers;
use \mvc\Models\Study as Study;
use \Exception as Exception;

class StudiesController extends \mvc\Lib\Controller
{
    public function setData ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $createStudy = array(
            'study_instance_uid' => $getContent['study_instance_uid'], 
            'study_description' => $getContent['study_description']
        );

        try {
            $user = Study::create($createStudy);

            $data = $this->tr($user);
            return  $response->withJson($data, 201);
        }catch(Exception $e){
            return  $response->withJson($e->getMessage(), 500);
        }

    }

    public function getDataByPatient ( $request,  $response, $args) {

        try {
            $user = Study::where('registro_id', '=', $args['id'])->get();
            $nameFile = $user[0]->study_instance_uid;
            $dirFile = "archivos/$nameFile.pdf";
            if (file_exists($dirFile)) {
                $user[0]->file_existe = 1;
            } else {
                $user[0]->file_existe = 0;
            }

            $data = $this->tr($user);
            return  $response->withJson($data);
        }catch(Exception $e){
            return  $response->withJson($e->getMessage(), 500);
        }

    }
}