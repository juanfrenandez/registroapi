<?php
namespace mvc\Controllers;

use \mvc\Models\Dato as Dato;

use \mvc\Models\Usuario as Usuario;

use \mvc\Models\Operacion as Operacion;

class OperacionesController extends \mvc\Lib\Controller
{

    public function setOperaciones ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userData = $request->getParsedBody();

        if ($userData['tag'] == null) {
            $resultado = $this->tr(null, true, 'Ingresa el tag');

            return $response->withJson($resultado, 404); 
        }

        $maxData = Dato::where('registro_id', $userDB->id)->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Primero ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        $countData = Operacion::where('dato_id', $maxData->id)->count();

        $userRegData = Operacion::create([
            'tag' => $userData['tag'], 
            'dato_id' => $maxData->id
        ]);

        $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function setOperacionesAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);

        }

        $userData = $request->getParsedBody();

        if ($userData['tag'] == null || $userData['empresa'] == null || $userData['inicio'] == null || $userData['termino'] == null) {
            $resultado = $this->tr(null, true, 'Ingresa los datos');

            return $response->withJson($resultado, 404); 
        }

        $maxData = Dato::where('registro_id', $args['id'])->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Primero ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        // $countData = Operacion::where('dato_id', $maxData->id)->count();

        $userRegData = Operacion::create([
            'tag' => $userData['tag'], 
            'empresa' => $userData['empresa'],
            'inicio' => $userData['inicio'],
            'termino' => $userData['termino'],
            'dato_id' => $maxData->id
        ]);

        $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function getOperaciones ( $request, $response, $args) {

       $mail = Operacion::all();

       if ($mail == null) {
           $resultado = $this->tr($mail, true, 'No existen registros');

            return  $response->withJson($resultado, 404);
       }

       $resultado = $this->tr($mail, false, '');

        return  $response->withJson($resultado);
    }

    public function putOperaciones ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Operacion::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos2');

            return $response->withJson($resultado,404);
        }

        if ($findRegistro->dato_id != $updateDB->id) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos3');

            return $response->withJson($resultado,404);
        }

        $findRegistro->tag = $updateForm['tag'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

    public function putOperacionesAdmin ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Operacion::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos2');

            return $response->withJson($resultado,404);
        }

        $findRegistro->tag = $updateForm['tag'];
        $findRegistro->empresa = $updateForm['empresa'];
        $findRegistro->inicio = $updateForm['inicio'];
        $findRegistro->termino = $updateForm['termino'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

    public function deleteOperaciones ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado);
        }

        $updateForm = $request->getParsedBody();

        $deleteDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateForm['id'] == null) {
            $resultado = $this->tr(null, true, 'Error al eliminar datos');

            return $response->withJson($resultado, 404);
        }

        $findRegistro = Operacion::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'Not found tag');

            return $response->withJson($resultado, 404);
        }

        if ($findRegistro->dato_id != $deleteDB->id) {
            $resultado = $this->tr(null, true, 'No se ha podido eliminar');

            return $response->withJson($resultado, 404);
        }

        $destroy = Operacion::destroy($findRegistro->id);

        $resultado = $this->tr(null, false, 'El registro datos ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

}
