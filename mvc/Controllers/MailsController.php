<?php
namespace mvc\Controllers;

use \mvc\Models\Dato as Dato;

use \mvc\Models\Usuario as Usuario;

use \mvc\Models\Mail as Mail;

class MailsController extends \mvc\Lib\Controller
{

    public function setMails ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userData = $request->getParsedBody();

        if ($userData['mail'] == null) {
            $resultado = $this->tr(null, true, 'Rellena todos los campos');

            return $response->withJson($resultado, 404);
        }

        $maxData = Dato::where('registro_id', $userDB->id)->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        $countData = Mail::where('dato_id', $maxData->id)->count();

        if ($userDB->membresia == 'premium') {

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 3 mails');

                return $response->withJson($resultado, 404);
            }


            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
            
        }elseif ($userDB->membresia == 'regular') {

            if ($countData >= 1) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 1 mail');

                return $response->withJson($resultado, 404);

            }

            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }elseif ($userDB->membresia == 'admin') {

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 1 mail');

                return $response->withJson($resultado, 404);

            }

            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }

        $resultado = $this->tr(null, true, 'error desconocido');

        return  $response->withJson($resultado, 201);
    }

    public function setMailsAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $userData = $request->getParsedBody();

        if ($userData['mail'] == null) {
            $resultado = $this->tr(null, true, 'Rellena todos los campos');

            return $response->withJson($resultado, 404);
        }

        $maxData = Dato::where('registro_id', $args['id'])->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        $countData = Mail::where('dato_id', $maxData->id)->count();

        $roles = Usuario::find($args['id']);

        if ($roles->membresia == 'premium') {

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 3 mails');

                return $response->withJson($resultado, 404);
            }


            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
            
        }elseif ($roles->membresia == 'regular') {

            if ($countData >= 1) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 1 mail');

                return $response->withJson($resultado, 404);

            }

            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }elseif ($roles->membresia == 'admin') {

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes agregar 1 mail');

                return $response->withJson($resultado, 404);

            }

            $userRegData = Mail::create([
                'mail' => $userData['mail'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegData, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }

        $resultado = $this->tr(null, true, 'error desconocido');

        return  $response->withJson($resultado, 201);
    }

    public function getMails ( $request, $response, $args) {

       $mail = Mail::all();

       if ($mail == null) {
           $resultado = $this->tr($mail, true, 'No existen registros');

            return  $response->withJson($resultado, 404);
       }

       $resultado = $this->tr($mail, false, '');

        return  $response->withJson($resultado);
    }

    public function putMails ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        if ($updateForm['mail'] == null) {

            $resultado = $this->tr(null, true, 'Rellena los datos');

            return $response->withJson($resultado, 404);
                    
        }        

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Mail::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'No se encontro registro');

            return $response->withJson($resultado,404);
        }

        if ($findRegistro->dato_id != $updateDB->id) {
            $resultado = $this->tr(null, true, 'Sin registro');

            return $response->withJson($resultado,404);
        }

        $findRegistro->mail = $updateForm['mail'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

    public function putMailsAdmin ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        if ($updateForm['mail'] == null) {

            $resultado = $this->tr(null, true, 'Rellena los datos');

            return $response->withJson($resultado, 404);
                    
        }        

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Mail::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'No se encontro registro');

            return $response->withJson($resultado,404);
        }

        $findRegistro->mail = $updateForm['mail'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

    public function deleteMails ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado);
        }

        $updateForm = $request->getParsedBody();

        $deleteDB = Dato::where('registro_id', $userDB->id)->first();

        if ($deleteDB == null) {
            $resultado = $this->tr(null, true, 'Error al eliminar datos');

            return $response->withJson($resultado, 404);
        }

        $findRegistro = Mail::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'No existe el registro');

            return $response->withJson($resultado, 404);
        }

        if ($findRegistro->dato_id != $deleteDB->id) {
            $resultado = $this->tr(null, true, 'Sin coincidencias');

            return $response->withJson($resultado, 404);
        }

        $destroy = Mail::destroy($findRegistro->id);

        $resultado = $this->tr(null, false, 'El registro datos ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

}
