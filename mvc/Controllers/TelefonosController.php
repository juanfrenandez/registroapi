<?php
namespace mvc\Controllers;

use \mvc\Models\Dato as Dato;

use \mvc\Models\Usuario as Usuario;

use \mvc\Models\Telefono as Telefono;

class TelefonosController extends \mvc\Lib\Controller
{

    public function setTelefonos ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userData = $request->getParsedBody();

        if ($userData['info'] == null || $userData['numero'] == null) {
            $resultado = $this->tr(null, true, 'Datos not found');

            return $response->withJson($resultado, 404); 
        }

        $maxData = Dato::where('registro_id', $userDB->id)->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Primero ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia == 'premium') {
            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes crear 3 registros telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
        }elseif ($userDB->miembro == 'regular') {

            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 1) {
                $resultado = $this->tr(null, true, 'Solo puedes crear un registro telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }elseif ($userDB->membresia == 'admin') {

            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes crear 4 registro telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
        }

        $resultado = $this->tr(null, true, $userDB);

        return  $response->withJson($resultado, 201);

    }

    public function setTelefonosAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $userData = $request->getParsedBody();

        if ($userData['info'] == null || $userData['numero'] == null) {
            $resultado = $this->tr(null, true, 'Datos not found');

            return $response->withJson($resultado, 404); 
        }

        $maxData = Dato::where('registro_id', $args['id'])->first();

        if ($maxData->id == null) {
            $resultado = $this->tr(null, true, 'Primero ingresa tus datos primarios');

            return $response->withJson($resultado, 404);
        }

        $roles = Usuario::find($args['id']);

        if ($roles->membresia == 'premium') {
            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes crear 3 registro telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
        }elseif ($roles->membresia == 'regular') {

            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 1) {
                $resultado = $this->tr(null, true, 'Solo puedes crear 1 un registro telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);

        }elseif ($roles->membresia == 'admin') {

            $countData = Telefono::where('dato_id', $maxData->id)->count();

            if ($countData >= 3) {
                $resultado = $this->tr(null, true, 'Solo puedes crear 3 registros telefono');

                return $response->withJson($resultado, 404);
            }

            $userRegData = Telefono::create([
                'info' => $userData['info'],  
                'numero' => $userData['numero'], 
                'dato_id' => $maxData->id
            ]);

            $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

            return  $response->withJson($resultado, 201);
        }

        $resultado = $this->tr(null, true, 'error desconocido');

        return  $response->withJson($resultado, 201);

    }

    public function getTelefonosAdmin ( $request, $response, $args) {

       $telefono = Telefono::all();

       $resultado = $this->tr($telefono, false, '');

        return  $response->withJson($resultado);
    }

    public function putTelefonos ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        if ($updateForm['id'] == null || $updateForm['info'] == null || $updateForm['numero'] == null) {
            $resultado = $this->tr(null, true, 'Rellena todos los datos');

            return $response->withJson($resultado, 404);
        }

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Telefono::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'No se registraron cambios');

            return $response->withJson($resultado,404);
        }

        if ($findRegistro->dato_id != $updateDB->id) {
            $resultado = $this->tr(null, true, 'Error al intentar cambiar los datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro->info = $updateForm['info'];
        $findRegistro->numero = $updateForm['numero'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

    public function putTelefonosAdmin ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {

            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        if ($updateForm['id'] == null || $updateForm['info'] == null || $updateForm['numero'] == null) {
            $resultado = $this->tr(null, true, 'Rellena todos los datos');

            return $response->withJson($resultado, 404);
        }

        $updateDB = Dato::where('registro_id', $userDB->id)->first();

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error al actualizar datos');

            return $response->withJson($resultado,404);
        }

        $findRegistro = Telefono::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'No se registraron cambios');

            return $response->withJson($resultado,404);
        }

        $findRegistro->info = $updateForm['info'];
        $findRegistro->numero = $updateForm['numero'];

        $findRegistro->save();

        $resultado = $this->tr($resultado, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 200);

    }

     public function deleteTelefonos ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado);
        }

        $updateForm = $request->getParsedBody();

        if ($updateForm == null) {
            $resultado = $this->tr(null, true, 'Sin registro de datos a borrar');

            return $response->withJson($resultado, 404);
        }

        $deleteDB = Dato::where('registro_id', $userDB->id)->first();

        if ($deleteDB == null) {
            $resultado = $this->tr(null, true, 'Error al eliminar datos');

            return $response->withJson($resultado, 404);
        }

        $findRegistro = Telefono::find($updateForm['id']);

        if ($findRegistro == null) {
            $resultado = $this->tr(null, true, 'Registro no encontrado');

            return $response->withJson($resultado, 404);
        }

        if ($findRegistro->dato_id != $deleteDB->id) {
            $resultado = $this->tr(null, true, 'Sin coincidencias');

            return $response->withJson($resultado, 404);
        }

        $destroy = Telefono::destroy($findRegistro->id);

        $resultado = $this->tr(null, false, 'El registro datos ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

}
