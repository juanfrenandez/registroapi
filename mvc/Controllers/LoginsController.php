<?php
namespace mvc\Controllers;

use \mvc\Models\Usuario as Usuario;
use \Firebase\JWT\JWT;

use \Exception as Exception;

class LoginsController extends \mvc\Lib\Controller
{

    public function setLogin ( $request,  $response, $args) {

    	$objeto = $request->getParsedBody();

        $resultado = [];
    	$userName = $objeto['user'];
    	$userPdw = $objeto['password'];
    	$userDB = Usuario::where('user', $userName)->first();

        if ($userDB == null) {
            $resultado = $this->tr(null, true, 'Credenciales invalidas');
             return $response->withJson($resultado, 404);
        }

    	$hash = $userDB->getPdw();

    	if (password_verify($userPdw, $hash)) {

            $key = "secret";
            $time = time();
            $nextWeek = time() + (7 * 24 * 60 * 60);
            $expireTime = date('Y-m-d', $nextWeek);
            $token = array(
                "typo_user" => $userDB['typo_user'],
                "user" => $userDB['user'],
                "patient_id" => $userDB['patient_id'],
                "id" => $userDB['id'],
                "iat" => $time,
                "exp" => $expireTime
            );

            $jwt = JWT::encode($token, $key);
            $resultado = $this->tr(array('active' => $userDB->active, 'user' => $userDB->getName(), 'token' => $jwt));

            $userDB->token = $jwt;
            $userDB->save();

		} else {
		     $resultado = $this->tr(null, true, 'Credenciales invalidas');
		};

        return $response->withJson($resultado)
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

    }

    public function destroyLogin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $userDB->token = null;

        $userDB->save();

    	$resultado = $this->tr(null, false, 'Sesion cerrada');

        return $response->withJson($resultado, 404);
    }
}