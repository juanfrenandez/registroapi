<?php
namespace mvc\Controllers;
use \mvc\Models\Series as Series;
use \Exception as Exception;

class SeriesController extends \mvc\Lib\Controller
{
    public function setData ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $createSeries = array(
            'serie_id' => $getContent['serie_id'],
            'study_id' => $getContent['study_id'],  
            'dicom_id' => $getContent['dicom_id']
        );

        try {
            $user = Series::create($createSeries);

            $data = $this->tr($user);
            return  $response->withJson($data, 201);
        }catch(Exception $e){
            return  $response->withJson($e->getMessage(), 500);
        }

    }

    public function getSeriesByStudy ( $request,  $response, $args) {

        $seriesById = ['study_id' => $args['id']];
        $series = Series::where($seriesById)->get();

        // $this->hiddenUser($users);

        $resultado = $this->tr($series,false, '');

        return $response->withJson($resultado);

    }
}