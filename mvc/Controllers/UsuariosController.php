<?php
namespace mvc\Controllers;

use \mvc\Models\Dato as Dato;

use \mvc\Models\Usuario as Usuario;

use \Firebase\JWT\JWT;

class UsuariosController extends \mvc\Lib\Controller
{
    public function getUsuarios ( $request,  $response, $args) {

        $users = Usuario::where('active', '=', 1)->get();

        // $this->hiddenUser($users);

        $resultado = $this->tr($users,false, '');

        return $response->withJson($resultado);

    }

    public function getAllPatients ( $request,  $response, $args) {

        $users = Usuario::where('typo_user', '=', 'Patient')
        ->join('studies', 'registro.id', '=', 'registro_id')
        ->select('registro.*', 'studies.study_description')
        ->get();

        // $this->hiddenUser($users);

        $resultado = $this->tr($users,false, '');

        return $response->withJson($resultado);

    }

    public function getPatientById ( $request,  $response, $args) {

        $patientById = ['id' => $args['id'], 'typo_user' => 'Patient'];
        $users = Usuario::where($patientById)->get();

        // $this->hiddenUser($users);

        $resultado = $this->tr($users,false, '');

        return $response->withJson($resultado);

    }

    public function getUsuariosDelete ( $request,  $response, $args) {

        $users = Usuario::onlyTrashed()->get();

        $resultado = $this->tr($users,false, '');

        return $response->withJson($resultado);

    }

    public function getUsuariosAdmin ( $request,  $response, $args) {

        $users = Usuario::all();

        // $this->hiddenUser($users);

        $resultado = $this->tr($users,false, '');

        return $response->withJson($resultado);

    }

    public function getUsuario ( $request,  $response, $args) {
    
        $user = Usuario::where('id',$args['id'])->with('dato.telefono','dato.mail','dato.operacion','educacion','direccion.horario')->first();

        if ($user->membresia == null || $user->membresia == 'inactivo') {
            
            return  $response->withJson($this->tr(null, true, 'Usuario not found1'), 404);

        }

       $resultado = $this->tr($user);

        return  $response->withJson($resultado);
    }

    public function getUsuarioByDicomId ( $request,  $response, $args) {
        
        $user = Usuario::where('dicom_id',$args['id'])->first();

        if (!$user) {
            return  $response->withJson($this->tr(409, false, null), 200);
        }

        $resultado = $this->tr($user);
        return  $response->withJson($resultado,200);
    }

    public function getCurrentUsuario ( $request,  $response, $args) {

        $user = Usuario::where('token',$args['token'])->with('dato.telefono','dato.mail','dato.operacion','educacion','direccion.horario')->first();

        if ($user == null || $user->token == null) {

            return  $response->withJson($this->tr(null, true, 'Not found 3'), 404);

        }

       $resultado = $this->tr($user);

        return  $response->withJson($resultado, 200);
    }

    public function getUsuarioById ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }
    
        $user = Usuario::where('id',$userDB->id)->with('dato.telefono','dato.mail','dato.operacion','educacion','direccion.horario')->first();

        if ($user->membresia == null || $user->membresia == 'inactivo') {
            
            return  $response->withJson($this->tr(null, true, 'Usuario not found3'), 404);

        }

       $resultado = $this->tr($user);

        return  $response->withJson($resultado,200);
    }

    public function sortUsuarios ( $request,  $response, $args) {

        $users = Usuario::with('dato')
                ->leftjoin('dato')
                ->orderBy('user', 'asc')
                ->get();

        $this->hiddenUser($users);

        if($users == null){

             return  $response->withJson($this->tr(null, true, 'Usuario not found'), 404);

        }

       $resultado = $this->tr($users);

        return  $response->withJson($users);
    }

    public function searchUsuarios ( $request,  $response, $args) {

        if ($args['search'] == null) {

            $resultado = $this->tr(null, true, 'Ingresa una busqueda');

            return $response->withJson($resultado);

        }

        // Funcion que buscará más especificamente por nombre agregados
        // $busqueda = [];

        // $array = '';
        //$consultas = explode(' ', $args['search']);
        // foreach ($consultas as $consulta) {

        //     function stripslashes_deep($value)
        //     {
        //         $value = is_array($value) ?
        //                     array_map('stripslashes_deep', $value) :
        //                     stripslashes($value);

        //         return $value;
        //     }

        //     $busqueda = Usuario::where('user', 'like', '%' . $consulta . '%')->get();

        //     $this->hiddenUser($busqueda);

        //     // Ejemplo
        //     $array .= $busqueda;
            
        // }

        // $busqueda = Usuario::where('user', 'like', '%' . $args['search'] . '%')->where('active', '=', 1)->get();
        $busqueda = Usuario::where('user', 'like', '%' . $args['search'] . '%')->get();

        $resultado = $this->tr( $busqueda );

        return $response->withJson($resultado);

    }

    public function getUsuariosByNumber ( $request,  $response, $args) {

        if ($args['pagination'] == null || $args['numberusers'] == null) {

            $resultado = $this->tr(null, true, 'Ingresa una busqueda');

            return $response->withJson($resultado);

        }

        $acountList = $args['numberusers'];

        $pagination = $args['pagination'];

        $initPagination = ($pagination - 1) * $acountList;

        $busqueda = Usuario::where('active', '=', 1)->skip($initPagination)->take($acountList)->get();

        $resultado = $this->tr( $busqueda );

        return $response->withJson($resultado);

    }

    public function getUsuariosByDate ( $request,  $response, $args) {

        if ($args['from'] == null || $args['to'] == null) {

            $resultado = $this->tr(null, true, 'Ingresa una busqueda');

            return $response->withJson($resultado);

        }

        $from = $args['from'] . ' 00:00:00';

        $to = $args['to'] . ' 11:59:59';

        $busqueda = Usuario::where('active', '=', 1)->whereBetween('created_at', [$from, $to])->get();

        $resultado = $this->tr( $busqueda );

        return $response->withJson($resultado);

    }

    public function postUsuario ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $validateUserDup = Usuario::where('user', $getContent['user'])->first();

        if ($validateUserDup != null) {

            $resultado = $this->tr(null, true, 'El Usuario que ingresaste ya existe');

            return $response->withJson($resultado, 409);

        }

        //validaciones comentadas para debbug
        // if ($getContent['user'] == null || $getContent['password'] == null || $getContent['membresia'] == null || $getContent['numero_cuenta'] == null ) {

        //     $resultado = $this->tr(null, true, 'Ingresa todos lo datos');

        //     return $response->withJson($resultado, 404);

        // }

        if ($getContent['membresia'] != 'premium' && $getContent['membresia'] != 'regular' && $getContent['membresia'] != 'inactivo') {
            $resultado = $this->tr(null, true, 'Ingresa un usuario valido');

            return $response->withJson($resultado);

        }

        $passEncripted = password_hash($getContent['password'], PASSWORD_DEFAULT);

        if ($getContent['membresia'] == 'admin') {
            $resultado = $this->tr(null, true, 'No puedes crear otro administrador');

            return $response->withJson($resultado);

        }

        $createUser = array(
            'user' => $getContent['user'], 
            'password' => $passEncripted, 
            'membresia' => $getContent['membresia'], 
            'numero_cuenta' => $getContent['numero_cuenta'],
            'register' => $getContent['register'],
            'active' => $getContent['active'],
            'dicom_id' => $getContent['dicom_id'],
            'patient_id' => $getContent['patient_id'],
            'typo_user' => $getContent['typo_user'],
            'studies' => $getContent['studies'],
            'license_id' => $getContent['license_id'],
            'expire_license' => $getContent['expire_license']
        );

        $user = Usuario::create($createUser);

        if($user == null){
             return  $response->withJson($this->tr(null, true, 'No se ha subido el usuario'), 404);
        }

        $data = $this->tr($user);
        return  $response->withJson($data, 201);

    }

    public function deleteUsuario ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        if ($getContent['id'] == null) {

            $data = $this->tr('Ingresa el usuario a eliminar');

            return  $response->withJson($data, 200);
        }

        $userDelete = Usuario::where('id', $getContent['id'])->first();

        $userDelete->delete();

        if($userDelete){

            $data = $this->tr('El usuario fue eliminado correctamente!');

            return  $response->withJson($data, 200);

        }

        return  $response->withJson($this->tr(null, true, 'El usuario no existe o ya ha sido borrado anteriormente.'), 404);

    }

    public function restoreUsuario ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        if ($getContent['id'] == null) {

            $data = $this->tr('Ingresa el usuario a eliminar');

            return  $response->withJson($data, 200);
        }

        $userDelete = Usuario::withTrashed()->where('id', $getContent['id'])->restore();

        if($userDelete){

            $data = $this->tr('El usuario fue salvado correctamente!');

            return  $response->withJson($data, 200);

        }

        return  $response->withJson($this->tr(null, true, 'El usuario no existe o ya ha sido borrado anteriormente.'), 404);

    }

    public function updateUsuario ( $request,  $response, $args) {

        if ($args['id'] != $userDB->id) {

            $resultado = $this->tr(null, true, 'El usuario no es correcto');

            return  $response->withJson($resultado,404);
        }

        $getContent = $request->getParsedBody();

        $userRegistro = Usuario::where('id', $userDB->id)->first();

        $user = Usuario::find($userRegistro->getId());

        if($user == null){

             return  $response->withJson($this->tr(null, true, 'No se registraron cambios'), 404);
        }

        $user->user = $getContent['user'];

        $user->token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));

        $user->save();

        $resultado = $this->tr($user,false,'El usuario a actualizado sus datos correctamente');

        return $response->withJson($resultado);

    }

    public function updateCorreoUsuario ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $userRegistro = Usuario::where('dicom_id', $args['id'])->first();

        $user = Usuario::find($userRegistro->getId());

        if($user == null){
            return  $response->withJson($this->tr(null, true, 'No se registraron cambios'), 404);
        }

        $user->numero_cuenta = $getContent['correo'];
        $user->save();

        $resultado = $this->tr($user,false,'El usuario a actualizado sus datos correctamente');
        return $response->withJson($resultado);

    }

    public function updateUsuarioAdmin ( $request,  $response, $args) {

        $getContent = $request->getParsedBody();

        $userRegistro = Usuario::where('id', $userDB->id)->first();

        $user = Usuario::find($userRegistro->getId());

        if($user == null){

             return  $response->withJson($this->tr(null, true, 'No se registraron cambios'), 404);
        }

        $user->membresia = $getContent['membresia'];

        $user->save();

        $resultado = $this->tr($user,false,'El usuario a actualizado sus datos correctamente');

        return $response->withJson($resultado);

    }

}
