<?php
namespace mvc\Controllers;

use \mvc\Models\Direccion as Direccion;

use \mvc\Models\Usuario as Usuario;

class DireccionesController extends \mvc\Lib\Controller
{
    // public function getPacientes ( $request,  $response, $args) {
    
    //     $data = array('name' => 'no name');

    //     return  $response->withJson($data);
    // }

    public function setDireccion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $userForm = $request->getParsedBody();

        if ($userForm['calle'] == null || $userForm['numero'] == null || $userForm['colonia'] == null || $userForm['maps'] == null || $userForm['ciudad'] == null || $userForm['estado'] == null) {

                $resultado = $this->tr(null, true, 'Rellena todos los campos');

                return $response->withJson($resultado, 404);

        }

        $count = Direccion::where('registro_id', $userDB->id)->count();

        if ($userDB->membresia == 'premium') {
            
            if ($count >= 3) {
                $resultado = $this->tr(null, true, 'No puedes agregar mas direcciones');

                return $response->withJson($resultado, 404);
            }

        }

        if ($userDB->membresia == 'regular') {
            
            if ($count >= 1) {

                $resultado = $this->tr(null, true, 'No puedes agregar mas direcciones, para utilizar hasta 3 cambia tu membresia a premium');

                return $response->withJson($resultado, 404);

            }
           
        }

    	$userRegDireccion = Direccion::create([
            'calle' => $userForm['calle'], 
            'numero' => $userForm['numero'],
            'numero' => $userForm['maps'], 
            'colonia' => $userForm['colonia'],
            'ciudad' => $userForm['ciudad'],
            'estado' => $userForm['estado'],
            'registro_id' => $userDB->id
        ]);

        $resultado = $this->tr(null, false, 'Se ha añadido una nueva direccion');

        return  $response->withJson($resultado, 201);

    }

    public function getDireccion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $roles = Direccion::where('registro_id', $userDB->id)->get();

        $resultado = $this->tr($roles, false, '');

        return  $response->withJson($resultado, 201);
    }

    public function getDireccionAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {

            $resultado = $this->tr(null, false, 'Error al editar');

            return  $response->withJson($resultado, 201);

        }

        $roles = Direccion::where('registro_id', $args['id'])->get();

        $resultado = $this->tr($roles, false, '');

        return  $response->withJson($resultado, 201);
    }

    public function setDireccionAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);
        }

        $userForm = $request->getParsedBody();

        if ($userForm['calle'] == null || $userForm['numero'] == null || $userForm['colonia'] == null || $userForm['maps'] == null || $userForm['ciudad'] == null || $userForm['estado'] == null) {

                $resultado = $this->tr(null, true, 'Rellena todos los campos');

                return $response->withJson($resultado, 404);

        }

        $roles = Usuario::where('id', $args['id'])->count();

        $count = Direccion::where('registro_id', $roles)->count();

        if ($roles->membresia == 'premium') {
            
            if ($count >= 3) {
                $resultado = $this->tr(null, true, 'No puedes agregar mas direcciones');

                return $response->withJson($resultado, 404);
            }

        }

        if ($roles->membresia == 'regular') {
            
            if ($count >= 1) {

                $resultado = $this->tr(null, true, 'No puedes agregar mas direcciones, para utilizar hasta 3 cambia tu membresia a premium');

                return $response->withJson($resultado, 404);

            }
           
        }

        $userRegDireccion = Direccion::create([
            'calle' => $userForm['calle'], 
            'numero' => $userForm['numero'],
            'maps' => $userForm['maps'],
            'colonia' => $userForm['colonia'],
            'ciudad' => $userForm['ciudad'],
            'estado' => $userForm['estado'],
            'registro_id' => $args['id']
        ]);

        $resultado = $this->tr(null, false, 'Se ha añadido una nueva direccion');

        return  $response->withJson($resultado, 201);

    }

    public function putDireccion ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        $direccionDB = Direccion::where('id', $args['id'])->first();

        if ($direccionDB == null) {
            $resultado = $this->tr(null, true, 'Educacion not found');
            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            if ($userDB->id != $direccionDB->registro_id) {
                $resultado = $this->tr(null, true, 'No se puede cambiar esta direccion');

                return $response->withJson($resultado, 404);
            }
        }

        $userForm = $request->getParsedBody();

        $direccionDB->calle = $userForm['calle'];
        $direccionDB->numero = $userForm['numero'];
        $direccionDB->maps = $userForm['maps'];
        $direccionDB->colonia = $userForm['colonia'];
        $direccionDB->ciudad = $userForm['ciudad'];
        $direccionDB->estado = $userForm['estado'];

        $direccionDB->save();

        $resultado = $this->tr($direccionDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 201);
    }

}
