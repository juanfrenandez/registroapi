<?php
namespace mvc\Controllers;

use \mvc\Models\Horario as Horario;

use \mvc\Models\Direccion as Direccion;

use \mvc\Models\Usuario as Usuario;

class HorariosController extends \mvc\Lib\Controller
{

    public function setHorarios ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

    	$userForm = $request->getParsedBody();

        if ($userForm['direccion_id'] == null || $userForm['dia'] == null || $userForm['inicio'] == null || $userForm['cierre'] == null) {

            $resultado = $this->tr(null, true, 'No se han agregado todos los registros');

            return $response->withJson($resultado, 404);
        }

        $direccion = Direccion::find($userForm['direccion_id']);

        if ($direccion == null) {
            $resultado = $this->tr(null, true, 'Direccion not found');

            return $response->withJson($resultado, 404); 
        }

        if ($direccion->registro_id != $userDB->id) {
            $resultado = $this->tr(null, true, 'Horarios not found');

            return $response->withJson($resultado, 404); 

        }

        $userRegDireccion = Horario::create([
            'dia' => $userForm['dia'],  
            'inicio' => $userForm['inicio'],
            'dia_final' => $userForm['dia_final'],
            'cierre' => $userForm['cierre'], 
            'direccion_id' => $userForm['direccion_id']
        ]);

        $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function setHorariosAdmin ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());

            return $response->withJson($resultado, 404);
        }

        if ($userDB->membresia != 'admin') {
            $resultado = $this->tr(null, true, 'not User');

            return $response->withJson($resultado, 404);

        }

        $userForm = $request->getParsedBody();

        if ($userForm['direccion_id'] == null || $userForm['dia'] == null || $userForm['inicio'] == null || $userForm['cierre'] == null) {

            $resultado = $this->tr(null, true, 'No se han agregado todos los registros');

            return $response->withJson($resultado, 404);
        }

        $direccion = Direccion::find($userForm['direccion_id']);

        if ($direccion == null) {
            $resultado = $this->tr(null, true, 'Direccion not found');

            return $response->withJson($resultado, 404); 
        }

        $userRegDireccion = Horario::create([
            'dia' => $userForm['dia'],  
            'inicio' => $userForm['inicio'],
            'dia_final' => $userForm['dia_final'],
            'cierre' => $userForm['cierre'], 
            'direccion_id' => $userForm['direccion_id']
        ]);

        $resultado = $this->tr($userRegDireccion, false, 'Los cambios se han realizado correctamente');

        return  $response->withJson($resultado, 201);
    }

    public function getHorarios ( $request, $response, $args) {

        $horario = Horario::all();

        $resultado = $this->tr($horario, false, '');

        return  $response->withJson($resultado);

    }

    public function getHorariosByDireccion ( $request, $response, $args) {

        $idDireccion = $args['id'];

        $horario = Horario::where('direccion_id', $args['id'])->get();

        $resultado = $this->tr($horario, false, '');

        return  $response->withJson($resultado);

    }

    public function updateHorarios ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Horario::find($updateForm['id']);

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error');

            return $response->withJson($resultado,404);
        }

        if ($updateForm['direccion_id'] == null ||  $updateForm['dia'] == null || $updateForm['inicio'] == null || $updateForm['cierre'] == null) {
            $resultado = $this->tr(null, true, 'Introduce todos los datos');

            return $response->withJson($resultado,404);
        }

        $validHorario = Direccion::find($updateDB['direccion_id']);

        if ($validHorario == null) {
            $resultado = $this->tr(null, true, 'Horario not found');

            return $response->withJson($resultado,404);
        }

        if ($validHorario->id != $updateDB->direccion_id) {
            
            $resultado = $this->tr(null, true, 'No puedes editar este horario');

            return $response->withJson($resultado, 404);

        }

        if ($validHorario->registro_id != $userDB->id) {

            $resultado = $this->tr(null, true, 'No se ha actualizado este Horario');

            return $response->withJson($resultado, 404);
        }

        $updateDB->dia = $updateForm['dia'];
        $updateDB->dia_final = $userForm['dia_final'];
        $updateDB->inicio = $updateForm['inicio'];
        $updateDB->cierre = $updateForm['cierre'];

        $updateDB->save();

        $resultado = $this->tr($updateDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 201);

    }

    public function updateHorariosAdmin ($request, $response, $args){

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado, 404);
        }

        $updateForm = $request->getParsedBody();

        $updateDB = Horario::find($updateForm['id']);

        if ($updateDB == null) {
            $resultado = $this->tr(null, true, 'Error');

            return $response->withJson($resultado,404);
        }

        if ($updateForm['direccion_id'] == null ||  $updateForm['dia'] == null || $updateForm['inicio'] == null || $updateForm['cierre'] == null) {
            $resultado = $this->tr(null, true, 'Introduce todos los datos');

            return $response->withJson($resultado,404);
        }

        $validHorario = Direccion::find($updateDB['direccion_id']);

        if ($validHorario == null) {
            $resultado = $this->tr(null, true, 'Horario not found');

            return $response->withJson($resultado,404);
        }

        if ($validHorario->id != $updateDB->direccion_id) {
            
            $resultado = $this->tr(null, true, 'No puedes editar este horario');

            return $response->withJson($resultado, 404);

        }

        $updateDB->dia = $updateForm['dia'];
        $updateDB->dia_final = $updateForm['dia_final'];
        $updateDB->inicio = $updateForm['inicio'];
        $updateDB->cierre = $updateForm['cierre'];

        $updateDB->save();

        $resultado = $this->tr($updateDB, false, 'Se ha actualizado correctamente');

        return  $response->withJson($resultado, 201);

    }    

    public function deleteHorarios ( $request,  $response, $args) {

        try {
            $userDB = $this->checkToken($request);
        } catch (\Exception $e) {
            $resultado = $this->tr(null, true, $e->getMessage());


            return $response->withJson($resultado);
        }

        $deleteForm = $request->getParsedBody();

        $deleteDB = Horario::find($deleteForm['id']);

        if ($deleteDB == null) {
            $resultado = $this->tr(null, true, 'Error al borrar Horario');

            return $response->withJson($resultado,404);
        }

        $validHorario = Direccion::find($deleteForm['direccion_id']);

        if ($validHorario->id != $deleteDB->direccion_id) {
            $resultado = $this->tr(null, true, 'No se pudo borrar el usuario');

            return $response->withJson($resultado, 404);
        }

        if ($validHorario->registro_id != $userDB->id) {
            $resultado = $this->tr(null, true, 'No se ha borrado esta Horario');

            return $response->withJson($resultado, 404);
        }

       $ifDestroy = Horario::destroy($deleteDB->id);

       $resultado = $this->tr(null, false, 'El registro Horario ha sido borrado');

        return  $response->withJson($resultado, 200);
    }

}
