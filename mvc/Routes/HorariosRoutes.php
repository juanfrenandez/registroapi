<?php

namespace mvc\Routes;

class HorariosRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        // $app->group('/direccion/{id}', function () {
        //     $this->map(['GET', 'DELETE', 'POST', 'PUT'], '', function ($request, $response, $args) {
        //         // Find, delete, patch or replace user identified by $args['id']
        //     })->setName('direccion');
        $app->post('/horarios', 'HorariosController:setHorarios')->setName('horarios.addHorarios');
        $app->post('/horarios-admin', 'HorariosController:setHorariosAdmin')->setName('horarios.addHorariosAdmin');
        $app->get('/horarios-admin/{id}', 'HorariosController:getHorariosByDireccion')->setName('horarios.getHorariosByDireccion');
        $app->get('/horarios', 'HorariosController:getHorarios')->setName('horarios.getHorarios');
        $app->delete('/horarios', 'HorariosController:deleteHorarios')->setName('horarios.deleteHorarios');
        $app->put('/horarios', 'HorariosController:updateHorarios')->setName('horarios.updateHorarios');
        $app->put('/horarios-admin', 'HorariosController:updateHorariosAdmin')->setName('horarios.updateHorariosAdmin');
        // });

    }
}