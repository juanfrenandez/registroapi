<?php

namespace mvc\Routes;

class StudiesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/studies', 'StudiesController:setData')->setName('data.studies');
        $app->get('/studies/patient/{id}', 'StudiesController:getDataByPatient')->setName('getData.studies');
    }
}