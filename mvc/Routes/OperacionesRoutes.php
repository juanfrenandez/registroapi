<?php

namespace mvc\Routes;

class OperacionesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    // function setRoutes($app){

    //     $app->post('/operaciones', 'OperacionesController:setOperaciones')->setName('datos.addOperaciones');
    //     $app->get('/operaciones', 'OperacionesController:getOperaciones')->setName('datos.getOperaciones');
    //     $app->put('/operaciones', 'OperacionesController:putOperaciones')->setName('datos.putOperaciones');
    //     $app->delete('/operaciones', 'OperacionesController:deleteOperaciones')->setName('educaciones.deleteOperaciones');
    // }

    function setRoutes($app){

        $app->group('/dato', function () {
            $this->map(['GET', 'DELETE', 'POST', 'PUT'], '', function ($request, $response, $args) {
                // Find, delete, patch or replace user identified by $args['id']
            })->setName('datos');
            $this->post('/operaciones', 'OperacionesController:setOperaciones')->setName('datos.addOperaciones');
            $this->post('/operaciones-admin/{id}', 'OperacionesController:setOperacionesAdmin')->setName('datos.addOperacionesAdmin');
            $this->get('/operaciones', 'OperacionesController:getOperaciones')->setName('datos.getOperaciones');
            $this->put('/operaciones', 'OperacionesController:putOperaciones')->setName('datos.putOperaciones');
            $this->put('/operaciones-admin', 'OperacionesController:putOperacionesAdmin')->setName('datos.putOperacionesAdmin');
            $this->delete('/operaciones', 'OperacionesController:deleteOperaciones')->setName('educaciones.deleteOperaciones');
        });

    }

}