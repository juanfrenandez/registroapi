<?php

namespace mvc\Routes;

class DatosRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        $app->post('/datos', 'DatosController:setDatos')->setName('datos.addDatos');
        $app->post('/datos-admin/{id}', 'DatosController:setDatosAdmin')->setName('datos.addDatosAdmin');
        $app->post('/upload', 'DatosController:uploadImage')->setName('datos.upload');
        $app->get('/datos', 'DatosController:getDatos')->setName('datos.getDatos');
        $app->put('/datos', 'DatosController:putDatos')->setName('datos.putDatos');
        $app->put('/datos/{id}', 'DatosController:putDatosAdmin')->setName('datos.putDatosAdmin');
        $app->delete('/datos', 'DatosController:deleteDatos')->setName('educaciones.deleteEducacion');
    }

}