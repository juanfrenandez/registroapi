<?php

namespace mvc\Routes;

class SeriesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/series', 'SeriesController:setData')->setName('data.series');
        $app->get('/series/study/{id}', 'SeriesController:getSeriesByStudy')->setName('data.seriesByStudy');
    }
}