<?php

namespace mvc\Routes;

use mvc\Middlewares\ValidateAdminMiddleware as ValidateAdminMiddleware;
use mvc\Middlewares\ValidateUserMiddleware as ValidateUserMiddleware;
use mvc\Middlewares\ValidateFormMiddleware as ValidateFromMiddleware;

class UsuariosRoutes
{

    protected $app=null;

    function __contruct($app){
        $this->app = $app;
    }

    public function setRoutes($app){

        $app->get('/usuarios', 'UsuariosController:getUsuarios')->setName('usuarios.getAll');
        $app->get('/usuario/search/{search}', 'UsuariosController:searchUsuarios')->setName('usuarios.search');
        $app->get('/usuario/date/{from}/{to}', 'UsuariosController:getUsuariosByDate')->setName('usuarios.searchByDate');
        $app->get('/usuarios/list/{pagination}/{numberusers}', 'UsuariosController:getUsuariosByNumber')->setName('usuarios.getByList');
        $app->get('/usuario/{id}', 'UsuariosController:getUsuario')->setName('usuarios.getById');
        $app->get('/usuario/dicom/{id}', 'UsuariosController:getUsuarioByDicomId')->setName('usuarios.getDicomById');
        $app->get('/usuario', 'UsuariosController:getUsuarioById')->setName('usuarios.getByIdUser')->add(new ValidateAdminMiddleware());
        $app->get('/current-usuario/{token}', 'UsuariosController:getCurrentUsuario')->setName('usuarios.getCurrentUser')->add(new ValidateUserMiddleware());
        $app->get('/usuarios-admin', 'UsuariosController:getUsuariosAdmin')->setName('usuarios.getByIdforAdmin')->add(new ValidateAdminMiddleware());
        $app->get('/usuarios-delete', 'UsuariosController:getUsuariosDelete')->setName('usuarios.getByIdforDelete');
        $app->post('/usuarios-save', 'UsuariosController:restoreUsuario')->setName('usuarios.getByIdforSave')->add(new ValidateAdminMiddleware());
        $app->post('/usuario', 'UsuariosController:postUsuario')->setName('usuarios.post')->add(new ValidateFromMiddleware());
        $app->delete('/usuario', 'UsuariosController:deleteUsuario')->setName('usuarios.delete')->add(new ValidateAdminMiddleware());
        $app->put('/usuario/{id}', 'UsuariosController:updateUsuario')->setName('usuarios.update')->add(new ValidateUserMiddleware());
        $app->put('/usuario-admin/{id}', 'UsuariosController:updateUsuarioAdmin')->setName('usuarios.updateAdmin')->add(new ValidateAdminMiddleware());
        $app->put('/usuario-mail/{id}', 'UsuariosController:updateCorreoUsuario')->setName('usuarios.updateCorreo');
        $app->get('/usuarios/sort', 'UsuariosController:sortUsuarios')->setName('usuarios.getSortByUser');
        $app->get('/patients', 'UsuariosController:getAllPatients')->setName('patients.getAllPatients');
        $app->get('/patients/{id}', 'UsuariosController:getPatientById')->setName('patients.getPatientById');

    }
}