<?php

namespace mvc\Routes;

class PruebaDicomRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/prueba', 'PruebaDicomController:getData')->setName('prueba.dicom');
        $app->post('/prueba-pixel', 'PruebaDicomController:getPixelData')->setName('image.dicom');
        $app->post('/pdf/studio/{id}', 'PruebaDicomController:setPDF')->setName('set.pdf');
        $app->post('/prueba/save', 'PruebaDicomController:saveDCM')->setName('set.dicom');
    }
}