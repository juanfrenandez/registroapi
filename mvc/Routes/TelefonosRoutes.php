<?php

namespace mvc\Routes;

class TelefonosRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        $app->post('/telefonos', 'TelefonosController:setTelefonos')->setName('datos.addTelefonos');
        $app->post('/telefonos-admin/{id}', 'TelefonosController:setTelefonosAdmin')->setName('datos.addTelefonosAdmin');
        $app->get('/telefonos', 'TelefonosController:getTelefonos')->setName('datos.getTelefonos');
        $app->put('/telefonos', 'TelefonosController:putTelefonos')->setName('datos.putTelefonos');
        $app->put('/telefonos-admin', 'TelefonosController:putTelefonosAdmin')->setName('datos.putTelefonosAdmin');
        $app->delete('/telefonos', 'TelefonosController:deleteTelefonos')->setName('educaciones.deleteTelefonos');
    }

}