<?php

namespace mvc\Routes;

class MailsRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        $app->post('/mails', 'MailsController:setMails')->setName('datos.addMails');
        $app->post('/mails-admin/{id}', 'MailsController:setMailsAdmin')->setName('datos.addMailsAdmin');
        $app->get('/mails', 'MailsController:getMails')->setName('datos.getMails');
        $app->put('/mails', 'MailsController:putMails')->setName('datos.putMails');
        $app->put('/mails-admin', 'MailsController:putMailsAdmin')->setName('datos.putMailsAdmin');
        $app->delete('/mails', 'MailsController:deleteMails')->setName('educaciones.deleteMails');
    }

}