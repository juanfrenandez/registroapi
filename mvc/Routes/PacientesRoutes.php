<?php

namespace mvc\Routes;

class PacientesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->get('/pacientes', 'PacientesController:getPacientes')->setName('pacientes.getAll');
    }
}