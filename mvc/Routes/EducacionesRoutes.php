<?php

namespace mvc\Routes;

class EducacionesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        $app->post('/educacion', 'EducacionesController:setEducacion')->setName('educaciones.addEducacion');
        $app->post('/educacion-admin/{id}', 'EducacionesController:setEducacionAdmin')->setName('educaciones.addEducacionAdmin');
        $app->get('/educacion', 'EducacionesController:getEducacion')->setName('educaciones.getEducacion');
        $app->put('/educacion', 'EducacionesController:updateEducacion')->setName('educaciones.updateEducacion');
        $app->delete('/educacion', 'EducacionesController:deleteEducacion')->setName('educaciones.deleteEducacion');
    }
}