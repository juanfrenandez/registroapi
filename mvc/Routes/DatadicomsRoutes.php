<?php

namespace mvc\Routes;

class DatadicomsRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/data-dicom', 'DatadicomsController:setData')->setName('data.dicom');
    }
}