<?php

namespace mvc\Routes;

class LoginsRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/login', 'LoginsController:setLogin')->setName('logins.login');
        $app->get('/logout', 'LoginsController:destroyLogin')->setName('logins.logout');
    }
}