<?php

namespace mvc\Routes;

class InstancesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){
        $app->post('/instances', 'InstancesController:setData')->setName('data.instances');
        $app->get('/instances/{user}', 'InstancesController:getData')->setName('data.instances');
        $app->get('/instances/{id}/file', 'InstancesController:getImage')->setName('data.image');
        $app->get('/instances/serie/{id}', 'InstancesController:getInstancesBySerie')->setName('data.instancesById');
        $app->post('/instances/image/{name}', 'InstancesController:setImage')->setName('image.setImage');
        $app->get('/instances/{id}/tags', 'InstancesController:getTags')->setName('data.tags');
    }
}