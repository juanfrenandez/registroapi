<?php

namespace mvc\Routes;

class DireccionesRoutes{

    protected $app=null;
    function __contruct($app){
        $this->app = $app;
    }

    function setRoutes($app){

        $app->post('/direccion', 'DireccionesController:setDireccion')->setName('direcciones.addDireccion');
        $app->post('/direccion-admin/{id}', 'DireccionesController:setDireccionAdmin')->setName('direcciones.addDireccionAdmin');
        $app->get('/direcciones', 'DireccionesController:getDireccion')->setName('direcciones.getDireccion');
        $app->get('/direcciones-admin/{id}', 'DireccionesController:getDireccionAdmin')->setName('direcciones.getDireccionAdmin');
        $app->put('/direcciones/{id}', 'DireccionesController:putDireccion')->setName('direcciones.putDireccion');
        // $app->get('/logout', 'LoginsController:destroyLogin')->setName('logins.logout');
    }
}